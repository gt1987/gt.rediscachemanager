基于stackexchange.redis的redis客户端封装
配置化
支持redis各种数据类型的基本操作
支持读写分离
支持在客户端层面实现的redis集群
支持多组redis集群 数据同步 异常切换
支持跨IDC的高可用双活方案


配置文件主目录 默认 为根目录下 Configs，若要修改，在app/web.config appsetting 节点下增加config:rediscachemanager=自定义路径
