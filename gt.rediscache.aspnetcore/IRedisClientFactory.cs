﻿using gt.rediscache.core.Clients;
using gt.rediscache.core.Entry;

namespace gt.rediscache.aspnetcore
{
    internal interface IRedisClientFactory
    {
        /// <summary>
        /// 创建 RedisClient
        /// </summary>
        SmartRedisClient CreateClient(string name);
        /// <summary>
        /// 创建 RedisClient
        /// </summary>
        SmartRedisClient CreateClient(SmartRedisClientConfiguration options);
    }
}
