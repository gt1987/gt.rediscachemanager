﻿using gt.rediscache.core;
using System;
using System.Collections.Generic;

namespace gt.rediscache.aspnetcore
{
    public interface IRedisClientManager : IDisposable
    {
        /// <summary>
        /// 获取所有得客户端
        /// </summary>
        /// <returns></returns>
        List<IRedisClient> GetAllClients();
        /// <summary>
        /// 是否存在Client
        /// </summary>
        /// <param name="clientName"></param>
        /// <returns></returns>
        bool ContainsClient(string clientName);
        /// <summary>
        /// 获取Client
        /// </summary>
        /// <param name="clientName"></param>
        /// <returns></returns>
        IRedisClient GetClient(string clientName);
    }
}
