﻿using gt.rediscache.logger;
using Microsoft.Extensions.Logging;
using System;

namespace gt.rediscache.aspnetcore
{
    /// <summary>
    /// .net core logger adapter
    /// </summary>
    public class LoggerAdapter : IRedisCacheLogger
    {
        private readonly ILogger<LoggerAdapter> _logger;

        public LoggerAdapter(ILogger<LoggerAdapter> logger)
        {
            _logger = logger;
        }

        public void Debug(string msg)
        {
            _logger.LogDebug(msg);
        }

        public void Error(string msg)
        {
            _logger.LogError(msg);
        }

        public void Error(Exception e)
        {
            Error(string.Empty, e);
        }

        public void Error(string msg, Exception e)
        {
            _logger.LogError(msg, e);
        }

        public void Info(string msg)
        {
            _logger.LogInformation(msg);
        }

        public void Warn(string msg)
        {
            _logger.LogWarning(msg);
        }
    }
}
