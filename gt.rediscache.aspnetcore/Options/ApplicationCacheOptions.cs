﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EM.RedisCache.AspNetCore.Options
{
    public class ApplicationCacheOptions
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 主 缓存池
        /// </summary>
        public string MainCache { get; set; }
        /// <summary>
        /// 备 缓存池
        /// </summary>
        public string BackupCache { get; set; }
        /// <summary>
        /// 待切换的 缓存节点名称
        /// </summary>
        public List<string> SwitchNodes { get; set; }
        /// <summary>
        /// 备用缓存切换功能开启
        /// </summary>
        public string BackupMode { get; set; }
        /// <summary>
        /// 延迟恢复时间
        /// useful in MultipleAndBackup mode
        /// </summary>
        public string DelayedRecoverySeconds { get; set; }
        /// <summary>
        /// 主从切换等待时间
        /// useful in MultipleAndBackup mode
        /// </summary>
        public string RedisFailoverWaitSeconds { get; set; }
    }
}
