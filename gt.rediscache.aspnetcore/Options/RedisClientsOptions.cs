﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EM.RedisCache.AspNetCore.Options
{
    public class RedisClientsOptions
    {
        /// <summary>
        /// 应用名称
        /// </summary>
        public string ApplicationName { get; set; }
        /// <summary>
        /// 应用缓存信息
        /// </summary>
        public List<ApplicationCacheOptions> ApplicationCaches { get; set; }
        /// <summary>
        /// 缓存配置路径
        /// </summary>
        public List<RedisNodePoolOptions> RedisNodePools { get; set; }
        /// <summary>
        /// 上报客户端驱动状态信息URL
        /// </summary>
        public string RedisCacheUrl { get; set; }
        /// <summary>
        /// 异常通知接口
        /// </summary>
        public string NotifyUrl { get; set; }
        /// <summary>
        /// OuYang = A,ZhouPu = B,Wanguo=E
        /// </summary>
        public string IDC { get; set; }
    }
}
