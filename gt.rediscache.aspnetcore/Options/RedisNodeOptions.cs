﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EM.RedisCache.AspNetCore.Options
{
    public class RedisNodeOptions
    {
        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 主Redis连接
        /// </summary>
        public string Master { get; set; }
        /// <summary>
        /// 从Redis连接，多个
        /// </summary>
        public List<string> Slaves { get; set; }
        /// <summary>
        /// slot From
        /// useful in Multipler
        /// </summary>
        public string SlotFrom { get; set; }
        /// <summary>
        /// slot End
        /// useful in Multipler
        /// </summary>
        public string SlotTo { get; set; }
        /// <summary>
        /// Redis实例，默认-1
        /// </summary>
        public string DB { get; set; }
        /// <summary>
        /// 连接密码
        /// </summary>
        public string Auth { get; set; }
    }
}
