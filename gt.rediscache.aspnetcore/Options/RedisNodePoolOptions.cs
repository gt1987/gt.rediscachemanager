﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EM.RedisCache.AspNetCore.Options
{
    public class RedisNodePoolOptions
    {
        /// <summary>
        /// 缓存池名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 缓存池模式
        /// 单连接模式，集群模式，Sentinel监控模式
        /// Single,Multiple,Sentinel
        /// </summary>
        public string Mode { get; set; }
        public List<RedisNodeOptions> Nodes { get; set; }
    }
}
