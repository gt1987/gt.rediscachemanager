﻿using EM.RedisCache.AspNetCore.Options;
using Microsoft.Extensions.Options;

namespace gt.rediscache.aspnetcore
{
    /// <summary>
    /// RedisClientOptions 验证
    /// </summary>
    public class RedisClientOptionsValidation : IValidateOptions<RedisClientsOptions>
    {
        /// <summary>
        /// 验证
        /// </summary>
        public ValidateOptionsResult Validate(string name, RedisClientsOptions options)
        {
            if (options == null
                    || options.ApplicationCaches == null
                    || options.ApplicationCaches.Count == 0)
                return ValidateOptionsResult.Fail("error redisclient options,please check option file!");

            return ValidateOptionsResult.Success;
        }
    }
}
