﻿using EM.RedisCache.AspNetCore.Options;
using gt.rediscache.core.Connections.ClientServer;
using gt.rediscache.logger;
using gt.rediscache.plugins;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;

namespace gt.rediscache.aspnetcore
{
    /// <summary>
    /// ServiceCollection 扩展
    /// </summary>
    public static class ServiceCollectionExtensions
    {
        /// <summary>
        /// 注册redis服务
        /// </summary>
        public static void AddEMRedis(this IServiceCollection collection, IConfiguration rootConfiguration, string configurationKey = "RedisClients")
        {
            collection.Configure<RedisClientsOptions>(rootConfiguration.GetSection(configurationKey));
            //仅会在没有同一类型实现的情况下才注册该服务
            collection.TryAddEnumerable(ServiceDescriptor.Singleton<IValidateOptions<RedisClientsOptions>, RedisClientOptionsValidation>());

            collection.AddLogging()
                .AddSingleton<RedisClientServerPoolFactory>()
                .AddSingleton(typeof(RedisClientStateReporter<>))
                .AddSingleton<RedisClientNotifyer>()
                .AddSingleton<RedisClientServerPoolFactory>()
                .AddSingleton<IRedisCacheLogger, LoggerAdapter>()
                .AddTransient<IRedisClientFactory, OptionsSmartRedisClientFactory>()
                .AddSingleton<IRedisClientManager, RedisClientManager>();
        }
    }
}
