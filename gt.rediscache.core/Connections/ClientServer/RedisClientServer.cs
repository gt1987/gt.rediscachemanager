﻿using gt.rediscache.core.Entry;
using gt.rediscache.core.Utility;
using gt.rediscache.logger;
using StackExchange.Redis;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace gt.rediscache.core.Connections.ClientServer
{
    public class RedisClientServer : IDisposable
    {
        private RedisNode _node;
        private IConnectionMultiplexer _connection;
        private IDatabase _database;
        public event EventHandler<ClientServerEventArgs> OnConnectionFailedEvent;
        public event EventHandler<ClientServerEventArgs> OnConnectionRestoredEvent;

        #region Construct

        public RedisClientServer(RedisNode node)
        {
            _node = node;
            _connection = ConnectionMultiplexer.Connect(node.RedisConnectOptions);
            if (!_connection.IsConnected)
            {
                RedisLogManager.Info(string.Format("redisclientserver init redisnode:{0} connection failed.please check internet.", node.ToString()));
            }
            _database = _connection.GetDatabase(node.DB);

            _connection.ConnectionFailed += M_connection_ConnectionFailed;
            _connection.ConnectionRestored += M_connection_ConnectionRestored;
        }

        #endregion

        /// <summary>
        /// Node元数据
        /// </summary>
        public RedisNode Node => _node;
        /// <summary>
        /// ClientServer名称，宇Node名称一致
        /// </summary>
        public string Name => _node.Name;
        /// <summary>
        /// Redis Connection
        /// </summary>
        public IConnectionMultiplexer Connection => _connection;

        #region event

        private void M_connection_ConnectionFailed(object sender, ConnectionFailedEventArgs e)
        {
            if (e.ConnectionType == ConnectionType.Subscription) return;

            try
            {
                IServer s = _connection.GetServer(e.EndPoint);
#if NET45
                if(s.IsSlave) return;
#else
                if (s.IsReplica) return;
#endif
                RedisLogManager.Info($"redisClientServer master:{RedisClientUtility.GetHostWithPort(e.EndPoint)} connection failed.connect type:{e.ConnectionType}");

                if (OnConnectionFailedEvent != null)
                    OnConnectionFailedEvent.Invoke(this, new ClientServerEventArgs() { MasterEndPoint = e.EndPoint, Health = false, Name = Name });
            }
            catch (Exception ex)
            {
                RedisLogManager.Error("HandlerConnectionFailed error.", ex);
            }
        }
        private void M_connection_ConnectionRestored(object sender, ConnectionFailedEventArgs e)
        {
            if (e.ConnectionType == ConnectionType.Subscription) return;
            try
            {
                Thread.Sleep(10);
                IServer s = _connection.GetServer(e.EndPoint);
#if NET45
                if(s.IsSlave) return;
#else
                if (s.IsReplica) return;
#endif
                RedisLogManager.Info($"redisClientServer master:{RedisClientUtility.GetHostWithPort(e.EndPoint)} connection restored.connect type:{e.ConnectionType}");

                if (OnConnectionRestoredEvent != null)
                    OnConnectionRestoredEvent?.Invoke(this, new ClientServerEventArgs() { MasterEndPoint = e.EndPoint, Health = true, Name = Name });
            }
            catch (Exception ex)
            {
                RedisLogManager.Error("HandlerConnectionRestored error.", ex);
            }
        }

        #endregion

        #region ExecuteCommand

        public EMRedisResult ExecuteCommand(Entry.RedisCommand command, RedisMessage message, CommandFlags commandFlags)
        {
            if (message == null) throw new ArgumentNullException("message");

            switch (command)
            {
                case RedisCommand.Info:
                    message.Result = EMRedisResult.Create(_connection.GetServer(message.CommonValue).InfoRaw());
                    break;
                case RedisCommand.ClientList:
                    message.Result = EMRedisResult.Create(_connection.GetServer(message.CommonValue).ClientList());
                    break;
                case RedisCommand.StringGet:
                    message.Result = EMRedisResult.Create(_database.StringGet(message.Key, commandFlags));
                    break;
                case RedisCommand.StringGetSet:
                    message.Result = EMRedisResult.Create(_database.StringGetSet(message.Key, message.StringValue.Value, commandFlags));
                    break;
                case RedisCommand.StringSet:
                    message.Result = EMRedisResult.Create(_database.StringSet(message.Key, message.StringValue.Value, message.ExpiredTime,
                        message.When, commandFlags));
                    break;
                case RedisCommand.StringSetArray:
                    message.Result = EMRedisResult.Create(_database.StringSet(message.StringValue.KeyValues, When.Always, commandFlags));
                    break;
                case RedisCommand.StringAppend:
                    message.Result = EMRedisResult.Create(_database.StringAppend(message.Key, message.StringValue.Value, commandFlags));
                    break;
                case RedisCommand.StringIncrement:
                    message.Result = EMRedisResult.Create(_database.StringIncrement(message.Key, message.StringValue.IncrOrDecrValue, commandFlags));
                    break;
                case RedisCommand.KeyExists:
                    message.Result = EMRedisResult.Create(_database.KeyExists(message.Key, commandFlags));
                    break;
                case RedisCommand.KeyRemove:
                    message.Result = EMRedisResult.Create(_database.KeyDelete(message.Key, commandFlags));
                    break;
                case RedisCommand.KeyRemoveArray:
                    message.Result = EMRedisResult.Create(_database.KeyDelete(message.Keys, commandFlags));
                    break;
                case RedisCommand.KeyExpired:
                    message.Result = EMRedisResult.Create(_database.KeyExpire(message.Key, message.ExpiredTime, commandFlags));
                    break;
                case RedisCommand.KeyTimeToLive:
                    message.Result = EMRedisResult.Create(_database.KeyTimeToLive(message.Key, commandFlags));
                    break;
                case RedisCommand.HashGet:
                    message.Result = EMRedisResult.Create(_database.HashGet(message.Key, message.HashValue.Field, commandFlags));
                    break;
                case RedisCommand.HashGetByArray:
                    message.Result = EMRedisResult.Create(_database.HashGet(message.Key, message.HashValue.Fields, commandFlags));
                    break;
                case RedisCommand.HashGetAll:
                    message.Result = EMRedisResult.Create(_database.HashGetAll(message.Key, commandFlags));
                    break;
                case RedisCommand.HashSet:
                    message.Result = EMRedisResult.Create(_database.HashSet(message.Key, message.HashValue.Field, message.HashValue.Value, message.When, commandFlags));
                    break;
                case RedisCommand.HashSetArray:
                    _database.HashSet(message.Key, message.HashValue.HashEntries, commandFlags);
                    message.Result = EMRedisResult.Create(true);
                    break;
                case RedisCommand.HashExist:
                    message.Result = EMRedisResult.Create(_database.HashExists(message.Key, message.HashValue.Field, commandFlags));
                    break;
                case RedisCommand.HashRemove:
                    message.Result = EMRedisResult.Create(_database.HashDelete(message.Key, message.HashValue.Field, commandFlags));
                    break;
                case RedisCommand.HashRemoveArray:
                    message.Result = EMRedisResult.Create(_database.HashDelete(message.Key, message.HashValue.Fields, commandFlags));
                    break;
                case RedisCommand.HashIncrement:
                    message.Result = EMRedisResult.Create(_database.HashIncrement(message.Key, message.HashValue.Field, message.HashValue.IncrOrDecrValue, commandFlags));
                    break;
                case RedisCommand.HashScan:
                    message.Result = EMRedisResult.Create(_database.HashScan(message.Key, message.ScanValue.FieldPattern ?? RedisValue.Null,
                        message.ScanValue.PageSize, message.ScanValue.Cursor, message.ScanValue.PageOffset, commandFlags));
                    break;
                case RedisCommand.HashFields:
                    message.Result = EMRedisResult.Create(_database.HashKeys(message.Key, commandFlags));
                    break;
                case RedisCommand.HashValues:
                    message.Result = EMRedisResult.Create(_database.HashValues(message.Key, commandFlags));
                    break;
                case RedisCommand.HashLength:
                    message.Result = EMRedisResult.Create(_database.HashLength(message.Key, commandFlags));
                    break;
                case RedisCommand.ListGetByIndex:
                    message.Result = EMRedisResult.Create(_database.ListGetByIndex(message.Key, message.ListValue.Index, commandFlags));
                    break;
                case RedisCommand.ListInsertAfter:
                    message.Result = EMRedisResult.Create(_database.ListInsertAfter(message.Key, message.ListValue.Pivot,
                        message.ListValue.Value, commandFlags));
                    break;
                case RedisCommand.ListInsertBefore:
                    message.Result = EMRedisResult.Create(_database.ListInsertBefore(message.Key, message.ListValue.Pivot,
                        message.ListValue.Value, commandFlags));
                    break;
                case RedisCommand.ListLeftPop:
                    message.Result = EMRedisResult.Create(_database.ListLeftPop(message.Key, commandFlags));
                    break;
                case RedisCommand.ListLeftPush:
                    message.Result = EMRedisResult.Create(_database.ListLeftPush(message.Key, message.ListValue.Value, When.Always, commandFlags));
                    break;
                case RedisCommand.ListLength:
                    message.Result = EMRedisResult.Create(_database.ListLength(message.Key, commandFlags));
                    break;
                case RedisCommand.ListRange:
                    message.Result = EMRedisResult.Create(_database.ListRange(message.Key, message.ListValue.FirstIndex,
                        message.ListValue.SecondIndex, commandFlags));
                    break;
                case RedisCommand.ListRightPop:
                    message.Result = EMRedisResult.Create(_database.ListRightPop(message.Key, commandFlags));
                    break;
                case RedisCommand.ListRightPush:
                    message.Result = EMRedisResult.Create(_database.ListRightPush(message.Key, message.ListValue.Value, message.When, commandFlags));
                    break;
                case RedisCommand.ListSetByIndex:
                    _database.ListSetByIndex(message.Key, message.ListValue.Index, message.ListValue.Value, commandFlags);
                    message.Result = EMRedisResult.Create(true);
                    break;
                case RedisCommand.ListTrim:
                    _database.ListTrim(message.Key, message.ListValue.FirstIndex, message.ListValue.SecondIndex, commandFlags);
                    message.Result = EMRedisResult.Create(true);
                    break;
                case RedisCommand.ListRemove:
                    message.Result = EMRedisResult.Create(_database.ListRemove(message.Key, message.ListValue.Value, message.ListValue.Count, commandFlags));
                    break;
                case RedisCommand.LockTake:
                    message.Result = EMRedisResult.Create(_database.LockTake(message.Key, message.CommonValue, message.ExpiredTime.Value, commandFlags));
                    break;
                case RedisCommand.LockQuery:
                    message.Result = EMRedisResult.Create(_database.LockQuery(message.Key, commandFlags));
                    break;
                case RedisCommand.LockRelease:
                    message.Result = EMRedisResult.Create(_database.LockRelease(message.Key, message.CommonValue, commandFlags));
                    break;
                case RedisCommand.SetAdd:
                    message.Result = EMRedisResult.Create(_database.SetAdd(message.Key, message.SetValue.Value, commandFlags));
                    break;
                case RedisCommand.SetAddArray:
                    message.Result = EMRedisResult.Create(_database.SetAdd(message.Key, message.SetValue.Values, commandFlags));
                    break;
                case RedisCommand.SetCombine:
                    message.Result = EMRedisResult.Create(_database.SetCombine(message.SetValue.Operation, message.SetValue.First, message.SetValue.Second, commandFlags));
                    break;
                case RedisCommand.SetCombineAndStore:
                    message.Result = EMRedisResult.Create(_database.SetCombineAndStore(message.SetValue.Operation, message.SetValue.Destination,
                        message.SetValue.First, message.SetValue.Second, commandFlags));
                    break;
                case RedisCommand.SetCombineAndStoreArray:
                    message.Result = EMRedisResult.Create(_database.SetCombineAndStore(message.SetValue.Operation, message.SetValue.Destination, message.SetValue.Keys, commandFlags));
                    break;
                case RedisCommand.SetCombineArray:
                    message.Result = EMRedisResult.Create(_database.SetCombine(message.SetValue.Operation, message.SetValue.Keys, commandFlags));
                    break;
                case RedisCommand.SetContains:
                    message.Result = EMRedisResult.Create(_database.SetContains(message.Key, message.SetValue.Value, commandFlags));
                    break;
                case RedisCommand.SetLength:
                    message.Result = EMRedisResult.Create(_database.SetLength(message.Key, commandFlags));
                    break;
                case RedisCommand.SetMembers:
                    message.Result = EMRedisResult.Create(_database.SetMembers(message.Key, commandFlags));
                    break;
                case RedisCommand.SetMove:
                    message.Result = EMRedisResult.Create(_database.SetMove(message.SetValue.Source, message.SetValue.Destination, message.SetValue.Value, commandFlags));
                    break;
                case RedisCommand.SetPop:
                    message.Result = EMRedisResult.Create(_database.SetPop(message.Key, commandFlags));
                    break;
                case RedisCommand.SetRandomMember:
                    message.Result = EMRedisResult.Create(_database.SetRandomMember(message.Key, commandFlags));
                    break;
                case RedisCommand.SetRandomMembers:
                    message.Result = EMRedisResult.Create(_database.SetRandomMembers(message.Key, message.SetValue.Count, commandFlags));
                    break;
                case RedisCommand.SetRemove:
                    message.Result = EMRedisResult.Create(_database.SetRemove(message.Key, message.SetValue.Value, commandFlags));
                    break;
                case RedisCommand.SetRemoveArray:
                    message.Result = EMRedisResult.Create(_database.SetRemove(message.Key, message.SetValue.Values, commandFlags));
                    break;
                case RedisCommand.SortedSetAdd:
                    message.Result = EMRedisResult.Create(_database.SortedSetAdd(message.Key, message.SortedSetValue.Member, message.SortedSetValue.Score,
                        message.When, commandFlags));
                    break;
                case RedisCommand.SortedSetDecrement:
                    message.Result = EMRedisResult.Create(_database.SortedSetDecrement(message.Key, message.SortedSetValue.Member,
                        message.SortedSetValue.IncreOrDecreValue, commandFlags));
                    break;
                case RedisCommand.SortedSetIncrement:
                    message.Result = EMRedisResult.Create(_database.SortedSetIncrement(message.Key, message.SortedSetValue.Member,
                        message.SortedSetValue.IncreOrDecreValue, commandFlags));
                    break;
                case RedisCommand.SortedSetLength:
                    message.Result = EMRedisResult.Create(_database.SortedSetLength(message.Key, message.SortedSetValue.Min, message.SortedSetValue.Max,
                        message.SortedSetValue.Exclude, commandFlags));
                    break;
                case RedisCommand.SortedSetRangeByRank:
                    message.Result = EMRedisResult.Create(_database.SortedSetRangeByRank(message.Key, message.SortedSetValue.Start, message.SortedSetValue.Stop,
                        message.SortedSetValue.Order, commandFlags));
                    break;
                case RedisCommand.SortedSetRangeByScore:
                    message.Result = EMRedisResult.Create(_database.SortedSetRangeByScore(message.Key, message.SortedSetValue.ScoreStart, message.SortedSetValue.ScoreStop,
                        message.SortedSetValue.Exclude, message.SortedSetValue.Order, message.SortedSetValue.Skip, message.SortedSetValue.Take, commandFlags));
                    break;
                case RedisCommand.SortedSetRemove:
                    message.Result = EMRedisResult.Create(_database.SortedSetRemove(message.Key, message.SortedSetValue.Member, commandFlags));
                    break;
                case RedisCommand.HyperLogLogAdd:
                    message.Result = EMRedisResult.Create(_database.HyperLogLogAdd(message.Key, message.HyperLogLogValue.Value, commandFlags));
                    break;
                case RedisCommand.HyperLogLogAddArray:
                    message.Result = EMRedisResult.Create(_database.HyperLogLogAdd(message.Key, message.HyperLogLogValue.Values, commandFlags));
                    break;
                case RedisCommand.HyperLogLogLength:
                    message.Result = EMRedisResult.Create(_database.HyperLogLogLength(message.Key, commandFlags));
                    break;
                case RedisCommand.HyperLogLogLengthArray:
                    message.Result = EMRedisResult.Create(_database.HyperLogLogLength(message.HyperLogLogValue.Keys, commandFlags));
                    break;
                case RedisCommand.HyperLogLogMerge:
                    _database.HyperLogLogMerge(message.HyperLogLogValue.Destination, message.HyperLogLogValue.First, message.HyperLogLogValue.Second, commandFlags);
                    message.Result = EMRedisResult.Create(true);
                    break;
                case RedisCommand.HyperLogLogMergeArray:
                    _database.HyperLogLogMerge(message.HyperLogLogValue.Destination, message.HyperLogLogValue.Keys, commandFlags);
                    message.Result = EMRedisResult.Create(true);
                    break;
                case RedisCommand.StringBitCount:
                    message.Result = EMRedisResult.Create(_database.StringBitCount(message.Key, message.BitValue.Start, message.BitValue.End, commandFlags));
                    break;
                case RedisCommand.StringBitOperation:
                    message.Result = EMRedisResult.Create(_database.StringBitOperation(message.BitValue.Operation, message.BitValue.Destination, message.BitValue.First,
                        message.BitValue.Second, commandFlags));
                    break;
                case RedisCommand.StringBitOperationArray:
                    message.Result = EMRedisResult.Create(_database.StringBitOperation(message.BitValue.Operation, message.BitValue.Destination, message.BitValue.Keys, commandFlags));
                    break;
                case RedisCommand.StringBitPosition:
                    message.Result = EMRedisResult.Create(_database.StringBitPosition(message.Key, message.BitValue.Bit, message.BitValue.Start, message.BitValue.End, commandFlags));
                    break;
                case RedisCommand.StringGetBit:
                    message.Result = EMRedisResult.Create(_database.StringGetBit(message.Key, message.BitValue.Offset, commandFlags));
                    break;
                case RedisCommand.StringSetBit:
                    message.Result = EMRedisResult.Create(_database.StringSetBit(message.Key, message.BitValue.Offset, message.BitValue.Bit, commandFlags));
                    break;
                default:
                    throw new InvalidOperationException("execute command do not support this:" + command.ToString());
            }
            return message.Result;
        }

        public async Task<EMRedisResult> ExecuteCommandAsync(Entry.RedisCommand command, RedisMessage message, CommandFlags commandFlags)
        {
            if (message == null) throw new InvalidOperationException("command get a error message type！");
            switch (command)
            {
                case RedisCommand.StringGet:
                    message.Result = await _database.StringGetAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringGetSet:
                    message.Result = await _database.StringGetSetAsync(message.Key, message.StringValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringSet:
                    message.Result = await _database.StringSetAsync(message.Key, message.StringValue.Value, message.ExpiredTime,
                        message.When, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringSetArray:
                    message.Result = await _database.StringSetAsync(message.StringValue.KeyValues, message.When, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringAppend:
                    message.Result = await _database.StringAppendAsync(message.Key, message.StringValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringIncrement:
                    message.Result = await _database.StringIncrementAsync(message.Key, message.StringValue.IncrOrDecrValue, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.KeyExists:
                    message.Result = await _database.KeyExistsAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.KeyRemove:
                    message.Result = await _database.KeyDeleteAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.KeyRemoveArray:
                    message.Result = await _database.KeyDeleteAsync(message.Keys, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.KeyExpired:
                    message.Result = await _database.KeyExpireAsync(message.Key, message.ExpiredTime, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.KeyTimeToLive:
                    message.Result = await _database.KeyTimeToLiveAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashGet:
                    message.Result = await _database.HashGetAsync(message.Key, message.HashValue.Field, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashGetByArray:
                    message.Result = await _database.HashGetAsync(message.Key, message.HashValue.Fields, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashGetAll:
                    message.Result = await _database.HashGetAllAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashSet:
                    message.Result = await _database.HashSetAsync(message.Key, message.HashValue.Field, message.HashValue.Value, message.When, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashSetArray:
                    await _database.HashSetAsync(message.Key, message.HashValue.HashEntries, commandFlags).ConfigureAwait(false);
                    message.Result = EMRedisResult.Create(true);
                    break;
                case RedisCommand.HashExist:
                    message.Result = await _database.HashExistsAsync(message.Key, message.HashValue.Field, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashRemove:
                    message.Result = await _database.HashDeleteAsync(message.Key, message.HashValue.Field, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashRemoveArray:
                    message.Result = await _database.HashDeleteAsync(message.Key, message.HashValue.Fields, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashIncrement:
                    message.Result = await _database.HashIncrementAsync(message.Key, message.HashValue.Field, message.HashValue.IncrOrDecrValue, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashFields:
                    message.Result = await _database.HashKeysAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashValues:
                    message.Result = await _database.HashValuesAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HashLength:
                    message.Result = await _database.HashLengthAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListGetByIndex:
                    message.Result = await _database.ListGetByIndexAsync(message.Key, message.ListValue.Index, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListInsertAfter:
                    message.Result = await _database.ListInsertAfterAsync(message.Key, message.ListValue.Pivot, message.ListValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListInsertBefore:
                    message.Result = await _database.ListInsertBeforeAsync(message.Key, message.ListValue.Pivot, message.ListValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListLeftPop:
                    message.Result = await _database.ListLeftPopAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListLeftPush:
                    message.Result = await _database.ListLeftPushAsync(message.Key, message.ListValue.Value, message.When, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListLength:
                    message.Result = await _database.ListLengthAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListRange:
                    message.Result = await _database.ListRangeAsync(message.Key, message.ListValue.FirstIndex, message.ListValue.SecondIndex, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListRightPop:
                    message.Result = await _database.ListRightPopAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListRightPush:
                    message.Result = await _database.ListRightPushAsync(message.Key, message.ListValue.Value, message.When, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListSetByIndex:
                    await _database.ListSetByIndexAsync(message.Key, message.ListValue.Index, message.ListValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(true);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListTrim:
                    await _database.ListTrimAsync(message.Key, message.ListValue.FirstIndex, message.ListValue.SecondIndex, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(true);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.ListRemove:
                    message.Result = await _database.ListRemoveAsync(message.Key, message.ListValue.Value, message.ListValue.Count, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetAdd:
                    message.Result = await _database.SetAddAsync(message.Key, message.SetValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetAddArray:
                    message.Result = await _database.SetAddAsync(message.Key, message.SetValue.Values, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetCombine:
                    message.Result = await _database.SetCombineAsync(message.SetValue.Operation, message.SetValue.First, message.SetValue.Second, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetCombineAndStore:
                    message.Result = await _database.SetCombineAndStoreAsync(message.SetValue.Operation, message.SetValue.Destination, message.SetValue.First,
                        message.SetValue.Second, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetCombineAndStoreArray:
                    message.Result = await _database.SetCombineAndStoreAsync(message.SetValue.Operation, message.SetValue.Destination, message.SetValue.Keys, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetCombineArray:
                    message.Result = await _database.SetCombineAsync(message.SetValue.Operation, message.SetValue.Keys, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetContains:
                    message.Result = await _database.SetContainsAsync(message.Key, message.SetValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetLength:
                    message.Result = await _database.SetLengthAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetMembers:
                    message.Result = await _database.SetMembersAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetMove:
                    message.Result = await _database.SetMoveAsync(message.SetValue.Source, message.SetValue.Destination, message.SetValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetPop:
                    message.Result = await _database.SetPopAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetRandomMember:
                    message.Result = await _database.SetRandomMemberAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetRandomMembers:
                    message.Result = await _database.SetRandomMembersAsync(message.Key, message.SetValue.Count, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetRemove:
                    message.Result = await _database.SetRemoveAsync(message.Key, message.SetValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SetRemoveArray:
                    message.Result = await _database.SetRemoveAsync(message.Key, message.SetValue.Values, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetAdd:
                    message.Result = await _database.SortedSetAddAsync(message.Key, message.SortedSetValue.Member, message.SortedSetValue.Score,
                        message.When, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetDecrement:
                    message.Result = await _database.SortedSetDecrementAsync(message.Key, message.SortedSetValue.Member, message.SortedSetValue.IncreOrDecreValue,
                        commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetIncrement:
                    message.Result = await _database.SortedSetIncrementAsync(message.Key, message.SortedSetValue.Member, message.SortedSetValue.IncreOrDecreValue,
                        commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetLength:
                    message.Result = await _database.SortedSetLengthAsync(message.Key, message.SortedSetValue.Min, message.SortedSetValue.Max,
                        message.SortedSetValue.Exclude, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetRangeByRank:
                    message.Result = await _database.SortedSetRangeByRankAsync(message.Key, message.SortedSetValue.Start, message.SortedSetValue.Stop,
                        message.SortedSetValue.Order, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetRangeByScore:
                    message.Result = await _database.SortedSetRangeByScoreAsync(message.Key, message.SortedSetValue.ScoreStart, message.SortedSetValue.ScoreStop,
                        message.SortedSetValue.Exclude, message.SortedSetValue.Order, message.SortedSetValue.Skip, message.SortedSetValue.Take, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.SortedSetRemove:
                    message.Result = await _database.SortedSetRemoveAsync(message.Key, message.SortedSetValue.Member, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HyperLogLogAdd:
                    message.Result = await _database.HyperLogLogAddAsync(message.Key, message.HyperLogLogValue.Value, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HyperLogLogAddArray:
                    message.Result = await _database.HyperLogLogAddAsync(message.Key, message.HyperLogLogValue.Values, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HyperLogLogLength:
                    message.Result = await _database.HyperLogLogLengthAsync(message.Key, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HyperLogLogLengthArray:
                    message.Result = await _database.HyperLogLogLengthAsync(message.HyperLogLogValue.Keys, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.HyperLogLogMerge:
                    await _database.HyperLogLogMergeAsync(message.HyperLogLogValue.Destination, message.HyperLogLogValue.First, message.HyperLogLogValue.Second,
                        commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(true);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.HyperLogLogMergeArray:
                    await _database.HyperLogLogMergeAsync(message.HyperLogLogValue.Destination, message.HyperLogLogValue.Keys, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(true);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringBitCount:
                    message.Result = await _database.StringBitCountAsync(message.Key, message.BitValue.Start, message.BitValue.End, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringBitOperation:
                    message.Result = await _database.StringBitOperationAsync(message.BitValue.Operation, message.BitValue.Destination, message.BitValue.First,
                        message.BitValue.Second, commandFlags).ContinueWith(t =>
                        {
                            return EMRedisResult.Create(t.Result);
                        }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringBitOperationArray:
                    message.Result = await _database.StringBitOperationAsync(message.BitValue.Operation, message.BitValue.Destination, message.BitValue.Keys, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringBitPosition:
                    message.Result = await _database.StringBitPositionAsync(message.Key, message.BitValue.Bit, message.BitValue.Start, message.BitValue.End, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringGetBit:
                    message.Result = await _database.StringGetBitAsync(message.Key, message.BitValue.Offset, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                case RedisCommand.StringSetBit:
                    message.Result = await _database.StringSetBitAsync(message.Key, message.BitValue.Offset, message.BitValue.Bit, commandFlags).ContinueWith(t =>
                    {
                        return EMRedisResult.Create(t.Result);
                    }).ConfigureAwait(false);
                    break;
                default:
                    throw new InvalidOperationException("execute async command do not support this:" + command.ToString());
            }
            return message.Result;
        }

        #endregion

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _connection.Dispose();
                _database = null;
                _connection = null;
            }
        }
    }
}
