﻿using gt.rediscache.core.Connections.Route;
using gt.rediscache.core.Entry;
using System.Collections.Concurrent;

namespace gt.rediscache.core.Connections.ClientServer
{
    /// <summary>
    /// ClientServerPool 工厂
    /// </summary>
    public class RedisClientServerPoolFactory
    {
        private ConcurrentDictionary<string, RedisClientServerPool> _poolContainer;

        public RedisClientServerPoolFactory()
        {
            _poolContainer = new ConcurrentDictionary<string, RedisClientServerPool>();
        }

        /// <summary>
        /// 获取RedisClientServerPool。如果不存在对应的RedisClientServerPool，则创建
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public RedisClientServerPool GetOrCreate(CachePoolConfiguration options)
        {
            return _poolContainer.GetOrAdd(options.Name, (x) =>
            {
                var routerFactory = new RedisNodeRouterFactory();
                return new RedisClientServerPool(options, routerFactory);
            });
        }
    }
}
