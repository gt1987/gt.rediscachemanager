﻿namespace gt.rediscache.core.Connections.Route
{
    public interface IRedisNodeRouter
    {
        /// <summary>
        /// 获取key指向的nodename
        /// </summary>
        string GetClientServerName(string key);
    }
}
