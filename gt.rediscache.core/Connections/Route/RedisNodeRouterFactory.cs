﻿using gt.rediscache.core.Entry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gt.rediscache.core.Connections.Route
{
    /// <summary>
    /// RedisNode路由Factory
    /// </summary>
    public class RedisNodeRouterFactory
    {
        public RedisNodeRouterFactory()
        { }

        /// <summary>
        /// 创建RedisNodeRouter
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IRedisNodeRouter CreateRouter(CachePoolConfiguration options)
        {
            IRedisNodeRouter router = null;
            switch (options.Mode)
            {
                case PoolMode.Single:
                    router = new SingleNodeRouter(new RouteData { NodeName = options.Nodes.First().Name });
                    break;
                case PoolMode.Multiple:
                    var routes = options.Nodes.Select(x => new RouteData { NodeName = x.Name, SlotFrom = x.SlotFrom, SlotTo = x.SlotTo }).ToList();
                    router = new MultipleNodeRouter(routes);
                    break;
                case PoolMode.Sentinel:
                    router = new SentinelNodeRouter();
                    break;
                default:
                    throw new ArgumentException("node pool mode error.");
            }
            return router;
        }
    }
}
