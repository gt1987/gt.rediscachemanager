﻿using System;

namespace gt.rediscache.core.Connections.Route
{
    public class SentinelNodeRouter : IRedisNodeRouter
    {
        public string GetClientServerName(string key)
        {
            throw new Exception("sentinel mode not support get node name by key.");
        }
    }
}
