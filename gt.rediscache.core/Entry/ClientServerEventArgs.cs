﻿using gt.rediscache.core.Connections.ClientServer;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace gt.rediscache.core.Entry
{
    /// <summary>
    /// ClientServer连接失败事件参数
    /// </summary>
    public class ClientServerFailedEventArgs : EventArgs
    {
        public EndPoint ServerEndPoint { get; set; }
    }
    /// <summary>
    /// ClientServer连接恢复事件参数
    /// </summary>
    public class ClientServerRestoredEventArgs : EventArgs
    {
        public EndPoint ServerEndPoint { get; set; }
    }
    /// <summary>
    /// ClientServer Event参数
    /// </summary>
    public class ClientServerEventArgs : EventArgs
    {
        /// <summary>
        /// clientserver name
        /// node name
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// clientserver 是否健康
        /// </summary>
        public bool Health { get; set; }
        /// <summary>
        /// 主连接EndPoint
        /// </summary>
        public EndPoint MasterEndPoint { get; set; }
    }
    public class ClientStateChangeEventArgs : EventArgs
    {
        /// <summary>
        /// RedisClient Name
        /// </summary>
        public string ClientName { get; set; }
        /// <summary>
        /// 0 初始化  
        /// 1 clientserver 切换
        /// 2 clientserver 恢复切回 
        /// 3 备client启用
        /// </summary>
        public int Type { get; set; }
        /// <summary>
        /// 发生切换或恢复的 clientserver
        /// 当Type=1或2时才会赋值
        /// </summary>
        public RedisClientServer ClientServer { get; set; }
    }
}
