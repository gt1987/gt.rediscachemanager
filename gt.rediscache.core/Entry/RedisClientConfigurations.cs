﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;

namespace gt.rediscache.core.Entry
{
    /// <summary>
    /// RedisClientOptions
    /// </summary>
    public class RedisClientConfiguration
    {
        /// <summary>
        /// 应用程序名称
        /// </summary>
        public string ApplicationName { get; set; }
        /// <summary>
        /// 机房标识
        /// </summary>
        public string IDC { get; set; }
        /// <summary>
        /// 连接客户端名称
        /// </summary>
        public string ClientName { get; set; }
        public CachePoolConfiguration Pool { get; set; }
    }
    /// <summary>
    /// 缓存池信息
    /// </summary>
    public class CachePoolConfiguration
    {
        /// <summary>
        /// 缓存池名称
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 异常恢复延迟时间
        /// </summary>
        public int? DelayedRecoverySeconds { get; set; }
        /// <summary>
        /// 异常等待恢复时间
        /// </summary>
        public int? RedisFailoverWaitSeconds { get; set; }
        /// <summary>
        /// 缓存池模式
        /// 单连接模式，集群模式，Sentinel监控模式
        /// </summary>
        public PoolMode Mode { get; set; }
        public List<RedisNode> Nodes { get; set; }
    }
    /// <summary>
    /// Redis连接node
    /// </summary>
    public class RedisNode
    {
        /// <summary>
        /// 节点名称
        /// </summary>
        public string Name { get; set; }
        public ConfigurationOptions RedisConnectOptions { get; set; }
        /// <summary>
        /// 主Redis连接
        /// </summary>
        public string Master { get; set; }
        /// <summary>
        /// 从Redis连接，多个
        /// </summary>
        public List<string> Slaves { get; set; }
        /// <summary>
        /// slot From
        /// useful in Multipler
        /// </summary>
        public int SlotFrom { get; set; }
        /// <summary>
        /// slot End
        /// useful in Multipler
        /// </summary>
        public int SlotTo { get; set; }
        /// <summary>
        /// Redis实例，默认-1
        /// </summary>
        public int DB { get; set; }
        public override string ToString()
        {
            StringBuilder s = new StringBuilder(Name);
            s.Append("(");
            s.Append(Master);
            if (Slaves != null && Slaves.Count != 0)
            {
                s.Append(",");
                s.Append(string.Join(",", Slaves));
            }
            s.Append(")");
            return s.ToString();
        }
    }
    /// <summary>
    /// SmartRedisClientOptions
    /// </summary>
    public class SmartRedisClientConfiguration : RedisClientConfiguration
    {
        /// <summary>
        /// 是否允许异常切换
        /// </summary>
        public bool AllowSwitch { get; set; }
        /// <summary>
        /// 消息是否同步
        /// </summary>
        public bool AllowSync { get; set; }
        public List<string> SwitchNodes { get; set; }
        /// <summary>
        /// 另外一个主 连接配置
        /// </summary>
        public RedisClientConfiguration BackupOptions { get; set; }
    }
    /// <summary>
    /// 缓存连接池模式
    /// </summary>
    public enum PoolMode
    {
        /// <summary>
        /// 默认单点
        /// </summary>
        Single,
        /// <summary>
        /// 多点主从
        /// </summary>
        Multiple,
        /// <summary>
        /// Sentinel模式
        /// </summary>
        Sentinel
    }
    /// <summary>
    /// 备 连接池模式
    /// </summary>
    public enum BackupModeEnum
    {
        /// <summary>
        /// 默认 不切换也不同步
        /// </summary>
        None = 0,
        /// <summary>
        /// 主备切换
        /// </summary>
        Switch = 1,
        /// <summary>
        /// 同步数据
        /// </summary>
        Sync = 2,
        /// <summary>
        /// 主备切换且同步数据
        /// </summary>
        SwitchAndSync = 3
    }
}
