﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gt.rediscache.core.Entry
{
    public class RedisClientInfo
    {
        public string Version { get; set; }
        /// <summary>
        /// 客户端应用名称
        /// </summary>
        public string ApplicatioinName { get; set; }
        public string ServerIp { get; set; }
        /// <summary>
        /// 缓存应用名称
        /// </summary>
        public string AppCacheName { get; set; }
        public bool SupportSync { get; set; }
        public bool SupportSwitch { get; set; }

        /// <summary>
        /// 延迟恢复时间
        /// useful in MultipleAndBackup mode
        /// </summary>
        public int DelayedRecoverySeconds { get; set; }

        /// <summary>
        /// 主从切换等待时间
        /// useful in MultipleAndBackup mode
        /// </summary>
        public int RedisFailoverWaitSeconds { get; set; }

        public RedisConnectCache ConnectionCache { get; set; }
        /// <summary>
        /// OuYang = A,ZhouPu = B,Wanguo=E
        /// </summary>
        public string IDC { get; set; }
    }
    public class RedisConnectCache
    {
        public string Name { get; set; }
        public List<RedisConnectNode> Nodes { get; set; }
        /// <summary>
        /// 连接是否被激活
        /// </summary>
        public bool HasActive { get; set; }
        public RedisConnectCache BackupConnectionCache { get; set; }

    }
    public class RedisConnectNode
    {
        public string Name { get; set; }
        public string Master { get; set; }
        public List<string> Slaves { get; set; }
        public int SlotFrom { get; set; }
        public int SlotTo { get; set; }
        private int db = -1;
        public int DB
        {
            get { return db; }
            set { db = value; }
        }
        /// <summary>
        /// 是否可用
        /// </summary>
        public bool Available { get; set; }

        public override string ToString()
        {
            StringBuilder s = new StringBuilder(Name);
            s.Append("(");
            string slaveIps = Slaves == null || Slaves.Count == 0 ? ""
                : string.Join(",", Slaves);
            s.Append(Master);
            if (!string.IsNullOrEmpty(slaveIps)) s.Append(",").Append(slaveIps);
            s.Append(")");
            return s.ToString();
        }
    }
}
