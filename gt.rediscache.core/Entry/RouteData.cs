﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gt.rediscache.core.Entry
{
    /// <summary>
    /// 路由数据对象
    /// </summary>
    internal class RouteData
    {
        public string NodeName { get; set; }
        public int SlotFrom { get; set; }
        public int SlotTo { get; set; }
    }
}
