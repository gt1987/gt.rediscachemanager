﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace gt.rediscache.core.Utility
{
    /// <summary>
    /// RedisValue Convert
    /// </summary>
    internal sealed class RedisValueConverter
    {
        public static RedisValue[] Convert(string[] values)
        {
            if (values == null || values.Length == 0) return null;
            return Array.ConvertAll(values, x => (RedisValue)x);
        }
        public static RedisKey[] ConvertToRedisKeyFromString(string[] values)
        {
            if (values == null || values.Length == 0) return null;
            return Array.ConvertAll(values, x => (RedisKey)x);
        }

        public static string[] Convert(RedisValue[] values)
        {
            if (values == null || values.Length == 0) return null;
            return Array.ConvertAll(values, x => (string)x);
        }

        public static HashEntry[] Convert(Dictionary<string, string> values)
        {
            if (values == null || values.Count == 0) return null;

            return Array.ConvertAll(values.ToArray(), x => new HashEntry(x.Key, x.Value));
        }

        public static Dictionary<string, string> Convert(HashEntry[] values)
        {
            if (values == null || values.Length == 0) return null;
            return Array.ConvertAll(values, x => new KeyValuePair<string, string>(x.Name, x.Value)).ToDictionary();
        }

        public static KeyValuePair<RedisKey, RedisValue>[] Convert(KeyValuePair<string, string>[] kvps)
        {
            if (kvps == null || kvps.Length == 0) return null;

            return Array.ConvertAll(kvps, x => new KeyValuePair<RedisKey, RedisValue>(x.Key, x.Value));
        }
    }
}
