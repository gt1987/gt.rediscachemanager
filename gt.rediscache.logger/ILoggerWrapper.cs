﻿using System;
using System.Collections.Generic;
using System.Text;

namespace gt.rediscache.logger
{
    public interface IRedisCacheLogger
    {
        /// <summary>
        /// 记录调试日志信息
        /// </summary>
        /// <param name="msg"></param>
        void Debug(string msg);
        /// <summary>
        /// 记录Warn日志
        /// </summary>
        /// <param name="msg">log message</param>
        void Warn(string msg);

        /// <summary>
        /// 记录信息日志信息
        /// </summary>
        /// <param name="msg">log message</param>
        void Info(string msg);

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg">log message</param>
        void Error(string msg);
        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="e"></param>
        void Error(Exception e);
        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="e"></param>
        void Error(string msg, Exception e);
    }
}
