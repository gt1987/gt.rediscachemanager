﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace gt.rediscache.logger
{
    public sealed class RedisLogManager
    {
        private static IRedisCacheLogger _redisLogger;

        static RedisLogManager()
        {
            _redisLogger = new TraceLogger();
        }

        /// <summary>
        /// 替换Logger
        /// </summary>
        /// <param name="loggerWrapper"></param>
        public static void ReplaceLogger(IRedisCacheLogger loggerWrapper)
        {
            _redisLogger = loggerWrapper;
        }

        /// <summary>
        /// 记录调试日志信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Debug(string msg)
        {
            _redisLogger.Debug(msg);
        }

        public static void Warn(string msg)
        {
            _redisLogger.Warn(msg);
        }

        /// <summary>
        /// 记录信息日志信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Info(string msg)
        {
            _redisLogger.Info(msg);
        }

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Error(string msg)
        {
            _redisLogger.Error(msg);
        }

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="e"></param>
        public static void Error(Exception e)
        {
            _redisLogger.Error(e);
        }
        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="e"></param>
        public static void Error(string msg, Exception e)
        {
            _redisLogger.Error(msg, e);
        }
    }
}
