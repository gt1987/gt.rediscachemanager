﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace gt.rediscache.logger
{
    public class TraceLogger : IRedisCacheLogger
    {
        private static System.Diagnostics.TraceSource _traceSource = new System.Diagnostics.TraceSource("RedisCache");

        /// <summary>
        /// 记录调试日志信息
        /// </summary>
        /// <param name="msg"></param>
        public void Debug(string msg)
        {
            _traceSource.TraceEvent(TraceEventType.Verbose, 0, msg);
        }
        /// <summary>
        /// 记录Warn日志
        /// </summary>
        /// <param name="msg">log message</param>
        public void Warn(string msg)
        {
            _traceSource.TraceEvent(TraceEventType.Warning, 0, msg);
        }

        /// <summary>
        /// 记录信息日志信息
        /// </summary>
        /// <param name="msg"></param>
        public void Info(string msg)
        {
            _traceSource.TraceEvent(TraceEventType.Information, 0, msg);
        }

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg"></param>
        public void Error(string msg)
        {
            _traceSource.TraceEvent(TraceEventType.Error, 0, msg);
        }

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="e"></param>
        public void Error(Exception e)
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine(e.Message);
            s.AppendLine(e.StackTrace);
            _traceSource.TraceEvent(TraceEventType.Error, 0, s.ToString());
        }
        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="e"></param>
        public void Error(string msg, Exception e)
        {
            StringBuilder s = new StringBuilder();
            s.AppendLine(msg);
            s.AppendLine(e.Message);
            s.AppendLine(e.StackTrace);

            _traceSource.TraceEvent(TraceEventType.Error, 0, s.ToString());
        }
    }
}
