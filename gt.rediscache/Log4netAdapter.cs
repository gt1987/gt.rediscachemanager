﻿using gt.rediscache.logger;
using log4net;
using log4net.Core;
using System;

namespace gt.rediscache
{
    /// <summary>
    /// log4etn 适配redislogger
    /// </summary>
    public class Log4netAdapter :IRedisCacheLogger
    {
        private readonly ILog _logger;

        public Log4netAdapter(ILog logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(ILog));
        }
        public void Debug(string msg)
        {
            _logger.Debug(msg);
        }

        public void Error(string msg)
        {
            _logger.Error(msg);
        }

        public void Error(Exception e)
        {
            _logger.Error(e);
        }

        public void Error(string msg, Exception e)
        {
            _logger.Error(msg, e);
        }

        public void Info(string msg)
        {
            _logger.Info(msg);
        }

        public void Warn(string msg)
        {
            _logger.Warn(msg);
        }
    }
}
