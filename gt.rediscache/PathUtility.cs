﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace gt.rediscache
{
    internal sealed class PathUtility
    {
        // Fields
        internal const int MAX_PATH_LENGTH = 260;

        /// <summary>
        /// Gets directory path
        /// </summary>
        /// <param name="path">Format: xxx/xxx/xxx/xxx.txt</param>
        /// <returns></returns>
        public static string GetDirectoryName(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return string.Empty;
            }
            int length = path.LastIndexOf('/');
            if (length == -1)
            {
                length = path.LastIndexOf('\\');
                if (length == -1) return path;
            }
            return path.Substring(0, length);
        }
        public static string GetDirectoryPureName(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return string.Empty;
            }
            int length = path.LastIndexOf('/') + 1;
            if (length == -1)
            {
                return path;
            }
            return path.Substring(length);
        }
        public static string GetFileName(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return string.Empty;
            }
            int length = path.LastIndexOf('/');
            if (length == -1)
            {
                length = path.LastIndexOf('\\');
                if (length == -1) return path;
            }
            return path.Substring(length + 1);
        }

        public static string GetFullPath(string fullPathOrRelativePath)
        {
            string path = fullPathOrRelativePath;
            if (IsFullPath(path))
            {
                return path;
            }
            if (fullPathOrRelativePath.StartsWith("~", StringComparison.CurrentCultureIgnoreCase))
            {
                path = path.TrimStart(new char[] { '~' });
            }
            return string.Format(@"{0}\{1}", AppDomain.CurrentDomain.BaseDirectory.TrimEnd(new char[] { '\\', '/' }), path.TrimStart(new char[] { '\\', '/' }).Replace("/", @"\"));
        }

        public static bool IsFileFullPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return false;
            }
            Regex regex = new Regex(@"^[A-Z]:\\{1,2}(([^\\/:\*\?<>\|]+)\\{1,2})+([^\\/:\*\?<>\|]+)(\.[A-Z]+)$", RegexOptions.IgnoreCase);
            Regex regex2 = new Regex(@"^\\{2}(([^\\/:\*\?<>\|]+)\\{1,2})+([^\\/:\*\?<>\|]+)(\.[A-Z]+)$", RegexOptions.IgnoreCase);
            return (regex.IsMatch(path) || regex2.IsMatch(path));
        }

        public static bool IsFullPath(string path)
        {
            if (string.IsNullOrEmpty(path))
            {
                return false;
            }
            Regex regex = new Regex(@"^[A-Z]:(\\{1,2}([^\\/:\*\?<>\|]*))*$", RegexOptions.IgnoreCase);
            Regex regex2 = new Regex(@"^(\\{1,2}([^\\/:\*\?<>\|]*))*$", RegexOptions.IgnoreCase);
            return (regex.IsMatch(path) || regex2.IsMatch(path));
        }

        public static string ToVirtualPath(string fullPath)
        {
            return string.Format("~/{0}", fullPath.Replace(@"\", "/").Replace(AppDomain.CurrentDomain.BaseDirectory.Replace(@"\", "/"), "").TrimStart(new char[] { '/' }));
        }

        public static string Combine(params string[] pathArray)
        {
            if ((pathArray == null) || (pathArray.Length == 0))
            {
                return string.Empty;
            }
            StringBuilder builder = new StringBuilder();
            foreach (string str in pathArray)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    if ((builder.Length > 0) && !str.StartsWith("\\", StringComparison.CurrentCultureIgnoreCase))
                    {
                        builder.Append("\\");
                    }
                    builder.Append(str.TrimEnd(new char[] { '/', '\\' }));
                }
            }
            return builder.ToString();
        }

        public static string UrlPathCombine(params string[] pathArray)
        {
            if ((pathArray == null) || (pathArray.Length == 0))
            {
                return string.Empty;
            }
            StringBuilder builder = new StringBuilder();
            foreach (string str in pathArray)
            {
                if (!string.IsNullOrEmpty(str))
                {
                    if ((builder.Length > 0) && !str.StartsWith("/", StringComparison.CurrentCultureIgnoreCase))
                    {
                        builder.Append("/");
                    }
                    builder.Append(str.TrimEnd(new char[] { '/', '\\' }));
                }
            }
            return builder.ToString();
        }

        public static string GetFullUrl(string rootPath, string virtualPath)
        {
            string path = virtualPath.TrimStart(new char[] { '~' });
            if (string.IsNullOrEmpty(rootPath)) return path.TrimStart(new char[] { '/', '\\' });
            return string.Format("{0}/{1}", rootPath.TrimEnd(new char[] { '/', '\\' }), path.TrimStart(new char[] { '/', '\\' }));
        }
    }
}
