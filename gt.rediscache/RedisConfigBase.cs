﻿namespace gt.rediscache
{
    public abstract class RedisConfigBase
    {
        public virtual string GetConfigKey()
        {
            return this.GetType().Name;
        }

        public virtual void ConfigChanged()
        {
        }
    }
}
