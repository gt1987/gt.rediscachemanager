﻿using System.Configuration;
using System.Runtime.Caching;

namespace gt.rediscache
{
    public class RedisConfigManager
    {
        private static ObjectCache cache = MemoryCache.Default;
        private static readonly string m_cacheConfigPath = "~/Configs/Redis_CacheManager.config";

        static RedisConfigManager()
        {
            var configValue = ConfigurationManager.AppSettings["EM:RedisCacheManager"];
            if (!string.IsNullOrEmpty(configValue))
            {
                m_cacheConfigPath = configValue;
            }
        }

        public static RedisCacheConfig GetRedisCacheConfigFromCache()
        {
            string fullPath = PathUtility.GetFullPath(m_cacheConfigPath);
            return GetConfigFromCacheWithPath<RedisCacheConfig>(new RedisCacheConfig().GetConfigKey(), fullPath);
        }

        public static T GetCachePoolFromCache<T>(string cacheKey)
            where T : RedisConfigBase, new()
        {
            RedisCacheConfig cacheConfig = GetRedisCacheConfigFromCache();
            if (cacheConfig == null || cacheConfig.ApplicationCaches == null) return default;
            var cachePoolConfig = cacheConfig.CachePathMappings.Find(e => string.Equals(e.Name, cacheKey));
            if (cachePoolConfig == null) return default;
            var fullPath = PathUtility.GetFullPath(cachePoolConfig.Path);
            return GetConfigFromCacheWithPath<T>(cacheKey, fullPath);
        }

        private static T GetConfigFromCacheWithPath<T>(string configKey, string fullPath)
            where T : RedisConfigBase, new()
        {
            if (string.IsNullOrEmpty(fullPath)) return default;
            T instance = (T)cache.Get(configKey);
            if (instance == null)
            {
                lock (typeof(T))
                {
                    instance = (T)cache.Get(configKey);
                    if (instance == null)
                    {
                        instance = InvokeGetConfig<T>(fullPath);
                        if (instance == null) return null;
                        string[] configFiles = new string[] { fullPath };
                        CacheItemPolicy policy = new CacheItemPolicy();
                        policy.Priority = CacheItemPriority.NotRemovable;
                        cache.Add(configKey, instance, policy);
                        HostFileChangeMonitor monitor = new HostFileChangeMonitor(configFiles);
                        monitor.NotifyOnChanged(new OnChangedCallback((o) =>
                        {
                            cache.Remove(configKey);
                            instance.ConfigChanged();
                        }));

                        policy.ChangeMonitors.Add(monitor);
                    }
                }
            }

            return instance ?? new T();
        }
        private static T InvokeGetConfig<T>(string fullPath) where T : class, new()
        {
            if (string.IsNullOrEmpty(fullPath)) return null;
            return (T)XmlConvertor.LoadXml(fullPath, typeof(T));
        }

    }
}
