﻿using System.Xml.Serialization;

namespace gt.rediscache
{
    /// <summary>
    /// 提供xml序列化与反序列化功能
    /// </summary>
    internal static class XmlConvertor
    {
        // XML序列化与反序列化
        // OBJECT -> XML
        public static void SaveXml(string filePath, object obj, System.Type type)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(filePath))
            {
                XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(type);
                xs.Serialize(writer, obj);
            }
        }

        // XML -> OBJECT
        public static object LoadXml(string filePath, System.Type type)
        {
            if (!System.IO.File.Exists(filePath))
                return null;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(filePath))
            {
                System.Xml.Serialization.XmlSerializer xs = new System.Xml.Serialization.XmlSerializer(type);
                object obj = xs.Deserialize(reader);
                return obj;
            }
        }
    }
}
