﻿using gt.rediscache;
using gt.rediscache.core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace gt.rediscachemanager.Test
{

    [TestClass]
    public class RedisCacheAsyncTest
    {
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestStringAsync()
        {
            string key = "s_test";
            string stringValue = "s_test_val";
            long longValue = 1;
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            client.KeyRemoveAsync(key).Wait();

            client.StringSetAsync(key, stringValue, TimeSpan.FromHours(1)).Wait();
            Assert.AreEqual(stringValue, client.StringGetAsync(key).Result);
            Assert.IsNotNull(client.KeyTimeToLiveAsync(key).Result);


            long appLength = client.StringAppendAsync(key, "_1").Result;
            Assert.AreEqual(appLength, stringValue.Length + 2);
            Assert.AreEqual(client.StringGet(key), string.Concat(stringValue, "_1"));

            string oldValue = client.StringGetSetAsync(key, longValue.ToString()).Result;
            Assert.AreEqual(string.Concat(stringValue, "_1"), oldValue);
            Assert.AreEqual(client.StringGet(key), longValue.ToString());

            long incrValue = client.StringIncrementAsync(key).Result;
            Assert.AreEqual(incrValue, 2.0);
            incrValue = client.StringIncrementAsync(key, TimeSpan.FromHours(1)).Result;
            Assert.AreEqual(incrValue, 3.0);
            Assert.IsNotNull(client.KeyTimeToLive(key));

        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestKeyAsync()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "k_test";
            client.KeyRemoveAsync(key).Wait();

            client.StringSet(key, "ok");
            bool flag = client.KeyExistsAsync(key).Result;
            Assert.IsTrue(flag);

            client.KeyExpiredAsync(key, TimeSpan.FromHours(1)).Wait();
            var timespan = client.KeyTimeToLiveAsync(key).Result;
            Assert.IsNotNull(timespan);

            client.KeyRemoveAsync(key).Wait();
            flag = client.KeyExistsAsync(key).Result;
            Assert.IsFalse(flag);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestHashAsync()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "h_test";
            client.KeyRemoveAsync(key).Wait();

            for (int i = 0; i < 50; i++)
            {
                Assert.IsTrue(client.HashSetAsync(key, "h_field_" + i.ToString(), "ok", TimeSpan.FromHours(1)).Result);
            }
            Assert.IsFalse(client.HashSetAsync(key, "h_field_0", "okok").Result);
            client.HashSet(key, "h_field_0", "ok");

            Dictionary<string, string> h = new Dictionary<string, string>();
            for (int i = 50; i < 100; i++)
            {
                h.Add("h_field_" + i.ToString(), "ok");
            }
            client.HashSetAsync(key, h).Wait();

            for (int i = 0; i < 100; i++)
            {
                string field = "h_field_" + i.ToString();
                string val = client.HashGetAsync(key, field).Result;
                Assert.AreEqual(val, "ok");
            }

            long length = client.HashLengthAsync(key).Result;
            Assert.AreEqual((int)length, 100);

            bool flag = client.HashExistsAsync(key, "h_field_0").Result;
            Assert.IsTrue(flag);

            var values = client.HashScan(key, "*_10").ToList();
            Assert.AreEqual(values.Count, 1);
            Assert.AreEqual(values[0].Name, "h_field_10");

            values = client.HashScan(key, "").ToList();
            Assert.AreEqual(values.Count, 100);

            List<Task> taskList = new List<Task>();
            for (int i = 0; i < 60; i++)
            {
                taskList.Add(client.HashRemoveAsync(key, "h_field_" + i.ToString()));
            }
            Task.WaitAll(taskList.ToArray());
            length = client.HashLengthAsync(key).Result;
            Assert.AreEqual((int)length, 40);

            string[] s_array = new string[20];
            for (int i = 60; i < 80; i++)
            {
                s_array[i - 60] = "h_field_" + i.ToString();
            }
            client.HashRemoveAsync(key, s_array).Wait();
            length = client.HashLengthAsync(key).Result;
            Assert.AreEqual((int)length, 20);

            string[] hashFields = client.HashFieldsAsync(key).Result;
            for (int i = 80; i < 100; i++)
            {
                Assert.AreEqual(hashFields[i - 80], "h_field_" + i.ToString());
            }

            var h_fileds = client.HashFieldsAsync(key).Result;
            Assert.AreEqual(h_fileds.Length, 20);

            var h_values = client.HashValuesAsync(key).Result;
            Assert.AreEqual(h_values.Length, 20);

            var h_all = client.HashGetAsync(key).Result;
            Assert.AreEqual(h_all.Count, 20);

            string[] hashValues = client.HashValuesAsync(key).Result;
            for (int i = 80; i < 100; i++)
            {
                Assert.AreEqual(hashValues[i - 80], "ok");
            }

            string key2 = "h_test_2";
            client.KeyRemove(key2);
            long inc_val = client.HashIncrementAsync(key2, "h_field").Result;
            Assert.AreEqual(inc_val, 1.0);

            inc_val = client.HashIncrementAsync(key2, "h_field", null, 5).Result;
            Assert.AreEqual(inc_val, 6.0);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestListAsync()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "l_test";
            client.KeyRemove(key);

            for (int i = 0; i < 100; i++)
            {
                client.ListLeftPushAsync(key, i.ToString()).Wait();
            }
            long length = client.ListLengthAsync(key).Result;
            Assert.AreEqual(length, 100);

            string val = client.ListGetAsync(key, 89).Result;
            Assert.AreEqual(val, "10");

            client.ListInsertAfterAsync(key, "10", "y").Wait();
            client.ListInsertBeforeAsync(key, "10", "x").Wait();
            length = client.ListLength(key);
            Assert.AreEqual(length, 102);
            string[] val_array12 = client.ListRangeAsync(key, 85, 102).Result;
            Assert.AreEqual(client.ListGet(key, 89), "x");
            Assert.AreEqual(client.ListGet(key, 90), "10");
            Assert.AreEqual(client.ListGet(key, 91), "y");


            val = client.ListLeftPopAsync(key).Result;
            length = client.ListLengthAsync(key).Result;
            Assert.AreEqual(length, 101);
            Assert.AreEqual(val, "99");

            val = client.ListRightPopAsync(key).Result;
            length = client.ListLength(key);
            Assert.AreEqual(length, 100);
            Assert.AreEqual(val, "0");

            string[] val_array = client.ListRange(key, 95, 98);
            Assert.AreEqual("5", val_array[0]);
            Assert.AreEqual("4", val_array[1]);
            Assert.AreEqual("3", val_array[2]);
            Assert.AreEqual("2", val_array[3]);

            length = client.ListRightPushAsync(key, "n").Result;
            Assert.AreEqual(length, 101);
            Assert.AreEqual("n", client.ListGet(key, 100));

            length = client.ListLeftPushAsync(key, "m").Result;
            Assert.AreEqual(length, 102);
            Assert.AreEqual("m", client.ListGet(key, 0));


            client.ListSetByIndexAsync(key, 0, "test").Wait();
            val = client.ListLeftPop(key);
            Assert.AreEqual(val, "test");

            client.ListTrimAsync(key, 0, 49).Wait();
            Assert.AreEqual(client.ListLength(key), 50);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestSet()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string set1 = "set1";
            string set2 = "set2";
            string set3 = "set3";
            string set4 = "set4";
            client.KeyRemoveAsync(set1).Wait();
            client.KeyRemoveAsync(set2).Wait();
            client.KeyRemoveAsync(set3).Wait();
            client.KeyRemoveAsync(set4).Wait();

            List<Task> tasks = new List<Task>();
            tasks.Add(client.SetAddAsync(set1, "1"));
            tasks.Add(client.SetAddAsync(set1, "2"));
            tasks.Add(client.SetAddAsync(set1, "3"));
            tasks.Add(client.SetAddAsync(set2, new string[] { "4", "5", "6" }));
            tasks.Add(client.SetAddAsync(set3, new string[] { "7", "8", "9" }));
            Task.WaitAll(tasks.ToArray());
            tasks.Clear();

            Assert.AreEqual(client.SetLengthAsync(set1).Result, client.SetLength(set2));
            Assert.AreEqual(client.SetLength(set2), 3);

            var result = client.SetCombineAsync(SetOperation.Union, new string[] { set1, set2, set3 }).Result;
            Assert.AreEqual(result.Length, 9);
            result = client.SetCombineAsync(SetOperation.Difference, new string[] { set1, set2, set3 }).Result;
            Assert.AreEqual(result.Length, 3);
            result = client.SetCombineAsync(SetOperation.Intersect, set1, set2).Result;
            Assert.IsNull(result);

            long length = client.SetCombineAndStoreAsync(SetOperation.Intersect, set4, new string[] { set1, set2, set3 }).Result;
            Assert.AreEqual(length, 0);
            length = client.SetCombineAndStoreAsync(SetOperation.Union, set4, set1, set2).Result;
            Assert.AreEqual(length, 6);

            Assert.IsFalse(client.SetContainsAsync(set4, "7").Result);
            Assert.IsTrue(client.SetContainsAsync(set4, "6").Result);

            result = client.SetMembersAsync(set4).Result;
            Assert.AreEqual(result.Length, 6);

            Assert.IsTrue(client.SetMoveAsync(set3, set4, "7").Result);
            Assert.IsTrue(client.SetContainsAsync(set4, "7").Result);
            Assert.IsFalse(client.SetContains(set3, "7"));

            var s = client.SetPopAsync(client.SetPop(set4)).Result;
            Assert.AreEqual(client.SetLength(set4), 6);

            s = client.SetRandomMemberAsync(set4).Result;
            Assert.AreEqual(client.SetLength(set4), 6);

            result = client.SetRandomMembersAsync(set4, 4).Result;
            Assert.AreEqual(client.SetLength(set4), 6);
            Assert.AreEqual(result.Length, 4);

            Assert.IsTrue(client.SetRemoveAsync(set4, "1").Result);
            Assert.AreEqual(client.SetLength(set4), 5);

            length = client.SetRemove(set4, new string[] { "2", "3" });
            Assert.AreEqual(client.SetLength(set4), 5 - length);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestSortedSet()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "k_sortedset";

            client.KeyRemove(key);

            for (int i = 1; i < 11; i++)
            {
                client.SortedSetAddAsync(key, i.ToString(), i).Wait();
            }
            Assert.AreEqual(client.SortedSetLengthAsync(key).Result, 10);

            var value = client.SortedSetIncrementAsync(key, "10", 1).Result;
            Assert.AreEqual(value, 11);
            value = client.SortedSetDecrementAsync(key, "10", 1).Result;
            Assert.AreEqual(value, 10);

            var result = client.SortedSetRangeByRankAsync(key, 0, 5).Result;
            Assert.AreEqual(result.Length, 6);
            result = client.SortedSetRangeByScoreAsync(key, 0, 5).Result;
            Assert.AreEqual(result.Length, 5);

            client.SortedSetRemove(key, "2");
            result = client.SortedSetRangeByRankAsync(key, 0, 5).Result;
            Assert.AreEqual(result.Length, 6);
            result = client.SortedSetRangeByScoreAsync(key, 0, 5).Result;
            Assert.AreEqual(result.Length, 4);
        }

        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestHyperLogLog()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key1 = "k_hyperloglog_test_1";
            string key2 = "k_hyperloglog_test_2";
            string key3 = "k_hyperloglog_test_3";
            string key4 = "k_hyperloglog_test_4";
            string key5 = "k_hyperloglog_test_5";
            client.KeyRemove(new string[] { key1, key2, key3, key4, key5 });

            for (int i = 0; i < 50; i++)
            {
                client.HyperLogLogAddAsync(key1, i.ToString()).Wait();
            }
            List<string> list = new List<string>();
            for (int i = 50; i < 100; i++)
            {
                list.Add(i.ToString());
            }
            client.HyperLogLogAddAsync(key1, list.ToArray()).Wait();

            var length = client.HyperLogLogLength(key1);
            Assert.AreEqual(length, 100);

            for (int i = 0; i < 100; i++)
            {
                client.HyperLogLogAddAsync(key2, i.ToString()).Wait();
            }

            client.HyperLogLogMerge(key3, key1, key2);
            Assert.AreEqual(client.HyperLogLogLengthAsync(key2).Result, 100);
            Assert.AreEqual(client.HyperLogLogLength(key3), 100);

            for (int i = 100; i < 200; i++)
            {
                client.HyperLogLogAdd(key4, i.ToString());
            }

            client.HyperLogLogMerge(key5, new string[] { key1, key4 });
            Assert.AreEqual(client.HyperLogLogLengthAsync(key4).Result, 100);
            //Assert.AreEqual(client.HyperLogLogLength(key5), 200);
        }

        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestBitmaps()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "k_bitmaps_test";
            string key1 = "k_bitmaps_test_1";
            string key2 = "k_bitmaps_test_2";
            string key3 = "k_bitmaps_test_3";
            string key4 = "k_bitmaps_test_4";
            client.KeyRemove(new string[] { key, key1, key2, key3, key4 });

            //10101010
            for (int i = 0; i < 16; i++)
            {
                var bit = i % 2 == 0;
                client.StringSetBitAsync(key, i, bit).Wait();
            }

            Assert.IsTrue(client.StringGetBitAsync(key, 0).Result);
            Assert.IsFalse(client.StringGetBit(key, 1));

            Assert.AreEqual(client.StringBitPositionAsync(key, true).Result, 0);
            Assert.AreEqual(client.StringBitPosition(key, false), 1);

            Assert.AreEqual(client.StringBitCountAsync(key).Result, 8);

            //01010101
            client.StringBitOperation(Bitwise.Not, key1, new string[] { key });
            Assert.AreEqual(client.StringBitCount(key1), 8);
            //00000000
            client.StringBitOperationAsync(Bitwise.And, key2, new string[] { key, key1 }).Wait();
            Assert.AreEqual(client.StringBitCountAsync(key2).Result, 0);
            //11111111
            client.StringBitOperation(Bitwise.Or, key3, new string[] { key, key1 });
            Assert.AreEqual(client.StringBitCount(key3), 16);
            //11111111
            client.StringBitOperationAsync(Bitwise.Xor, key4, new string[] { key, key1 }).Wait();
            Assert.AreEqual(client.StringBitCount(key4), 16);
        }

        //[DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        //[DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        //[DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        //[DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        //[TestMethod]
        //public void TestDataSync()
        //{
        //    CacheConfig cacheConfig = ConfigData.Instance.CacheConfig;
        //    ApplicationCache appCache = cacheConfig.ApplicationCaches.Find(e => !string.IsNullOrEmpty(e.BackupCache) && e.SyncDataToBackup);

        //    if (appCache == null)
        //    {
        //        //Assert.Inconclusive("请配置带有备缓存的缓存服务！");
        //        return;
        //    }
        //    ApplicationCache backupAppCache = cacheConfig.ApplicationCaches.Find(e => string.Equals(appCache.BackupCache, e.MainCache));
        //    if (backupAppCache == null)
        //    {
        //        //Assert.Inconclusive("请在ApplicationCache配置节，配置备用缓存！");
        //        return;
        //    }

        //    IRedisClient client = CacheManager.GetClient(appCache.Name);
        //    IRedisClient backupClient = CacheManager.GetClient(backupAppCache.Name);
        //    string key = "sync_test_";
        //    for (int i = 0; i < 100; i++)
        //    {
        //        client.StringSetAsync(key + i.ToString(), i.ToString());
        //    }

        //    for (int i = 0; i < 100; i++)
        //    {
        //        string val = backupClient.StringGetAsync(key + i.ToString()).Result;
        //        Assert.AreEqual(val, i.ToString());
        //    }
        //}
    }
}
