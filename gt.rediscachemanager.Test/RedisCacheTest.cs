﻿using gt.rediscache;
using gt.rediscache.core;
using gt.rediscache.core.Connections.ClientServer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;

namespace gt.rediscachemanager.Test
{
    [TestClass]
    public class RedisCacheTest
    {
        string withBackupCacheConfigKey = "cacheWithBackup";
        string singleCacheConfigKey = "singleCache";

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestConfigInit()
        {
            RedisCacheConfig cachConfig = RedisConfigManager.GetRedisCacheConfigFromCache();
            Assert.IsNotNull(cachConfig);
            Assert.IsNotNull(cachConfig.ApplicationCaches);
            Assert.IsNotNull(cachConfig.ApplicationCaches[0].MainCachePool);

            RedisCacheManager.InitCacheFromConfig();

            foreach (var appCache in cachConfig.ApplicationCaches)
            {
                IRedisClient client = RedisCacheManager.GetClient(appCache.Name);
                Assert.IsNotNull(client);
            }
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestGetClient()
        {
            IRedisClient userClient1 = RedisCacheManager.GetClient(withBackupCacheConfigKey);
            IRedisClient interClient1 = RedisCacheManager.GetClient(singleCacheConfigKey);

            IRedisClient userClient2 = RedisCacheManager.GetClient<CacheWithBackup>();
            IRedisClient interClient2 = RedisCacheManager.GetClient<SingleCache>();

            Assert.IsNotNull(userClient1);
            Assert.IsNotNull(interClient1);
            Assert.ReferenceEquals(userClient1, userClient2);
            Assert.ReferenceEquals(interClient1, interClient2);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestString()
        {
            string key = "str_test";
            string stringValue = "s_test_val";
            long longValue = 1;
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            client.KeyRemove(key);

            bool flag = client.StringSet(key, stringValue, TimeSpan.FromHours(1));
            Assert.IsTrue(flag);
            Assert.AreEqual(stringValue, client.StringGet(key));
            Assert.IsNotNull(client.KeyTimeToLive(key));

            long appLength = client.StringAppend(key, "_1");
            Assert.AreEqual(appLength, stringValue.Length + 2);
            Assert.AreEqual(client.StringGet(key), string.Concat(stringValue, "_1"));

            string oldValue = client.StringGetSet(key, longValue.ToString(), TimeSpan.FromHours(1));
            Assert.AreEqual(string.Concat(stringValue, "_1"), oldValue);
            Assert.AreEqual(client.StringGet(key), longValue.ToString());

            long incrValue = client.StringIncrement(key);
            Assert.AreEqual(incrValue, 2.0);
            incrValue = client.StringIncrement(key, TimeSpan.FromHours(1));
            Assert.AreEqual(incrValue, 3.0);
            Assert.IsNotNull(client.KeyTimeToLive(key));

            KeyValuePair<string, string>[] kvs = new KeyValuePair<string, string>[] {
                new KeyValuePair<string, string>("s_test_1","1"),
                new KeyValuePair<string, string>("s_test_2","2"),
                new KeyValuePair<string, string>("s_test_3","3")
            };
            client.KeyRemove(kvs.Select(e => e.Key.ToString()).ToArray());
            flag = client.StringSet(kvs);
            Assert.IsTrue(flag);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestKey()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "k_test";
            bool flag = client.KeyRemove(key);

            client.StringSet(key, "ok");
            flag = client.KeyExists(key);
            Assert.IsTrue(flag);

            client.KeyExpired(key, TimeSpan.FromHours(1));
            var timespan = client.KeyTimeToLive(key);
            Assert.IsNotNull(timespan);

            client.KeyRemove(key);
            flag = client.KeyExists(key);
            Assert.IsFalse(flag);

            List<KeyValuePair<string, string>> keys = new List<KeyValuePair<string, string>>();
            for (int i = 0; i < 3; i++)
            {
                keys.Add(new KeyValuePair<string, string>(keys + i.ToString(), "ok"));
            }

            client.StringSet(keys.ToArray());
            long l = client.KeyRemove(keys.Select(x => x.Key).ToArray());
            Assert.AreEqual(l, 3.0);

            if (client.LockTake("lock_1", "val1", TimeSpan.FromSeconds(10)))
            {
                var lockVal = client.LockQuery("lock_1");
                Assert.AreEqual(lockVal, "val1");
                client.LockRelease("lock_1", lockVal);
            }

            var info = client.RedisInfo(client.GetClientServers()[0].Node.Master);
            Assert.IsNotNull(info);
            var clientInfo = client.RedisClients(client.GetClientServers()[0].Node.Master);
            Assert.IsNotNull(clientInfo);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestHash()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "h_test";
            client.KeyRemove(key);

            for (int i = 0; i < 50; i++)
            {
                Assert.IsTrue(client.HashSet(key, "h_field_" + i.ToString(), "ok", TimeSpan.FromHours(1)));
            }
            Assert.IsFalse(client.HashSet(key, "h_field_0", "okok"));
            client.HashSet(key, "h_field_0", "ok");

            Dictionary<string, string> h = new Dictionary<string, string>();
            for (int i = 50; i < 100; i++)
            {
                h.Add("h_field_" + i.ToString(), "ok");
            }
            client.HashSet(key, h, TimeSpan.FromHours(1));
            Assert.IsNotNull(client.KeyTimeToLive(key));

            for (int i = 0; i < 100; i++)
            {
                string field = "h_field_" + i.ToString();
                string val = client.HashGet(key, field);
                Assert.AreEqual(val, "ok");
            }

            var h_fileds = client.HashFields(key);
            Assert.AreEqual(h_fileds.Length, 100);

            var h_values = client.HashValues(key);
            Assert.AreEqual(h_values.Length, 100);

            var h_all = client.HashGetAll(key);
            Assert.AreEqual(h_all.Count, 100);

            long length = client.HashLength(key);
            Assert.AreEqual((int)length, 100);

            bool flag = client.HashExists(key, "h_field_0");
            Assert.IsTrue(flag);

            var values = client.HashScan(key, "*_10").ToList();
            Assert.AreEqual(values.Count, 1);
            Assert.AreEqual(values[0].Name, "h_field_10");

            values = client.HashScan(key, "").ToList();
            Assert.AreEqual(values.Count, 100);

            for (int i = 0; i < 60; i++)
            {
                client.HashRemove(key, "h_field_" + i.ToString());
            }
            length = client.HashLength(key);
            Assert.AreEqual((int)length, 40);

            string[] s_array = new string[20];
            for (int i = 60; i < 80; i++)
            {
                s_array[i - 60] = "h_field_" + i.ToString();
            }
            client.HashRemove(key, s_array);
            length = client.HashLength(key);
            Assert.AreEqual((int)length, 20);

            string[] hashFields = client.HashFields(key);
            for (int i = 80; i < 100; i++)
            {
                Assert.AreEqual(hashFields[i - 80], "h_field_" + i.ToString());
            }

            string[] hashValues = client.HashValues(key);
            for (int i = 80; i < 100; i++)
            {
                Assert.AreEqual(hashValues[i - 80], "ok");
            }

            string key2 = "h_test_2";
            client.KeyRemove(key2);
            long inc_val = client.HashIncrement(key2, "h_field");
            Assert.AreEqual(inc_val, 1.0);

            inc_val = client.HashIncrement(key2, "h_field", null, 5);
            Assert.AreEqual(inc_val, 6.0);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestList()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "l_test";
            client.KeyRemove(key);

            for (int i = 0; i < 100; i++)
            {
                long l = client.ListLeftPush(key, i.ToString());
                Assert.AreEqual(l, i + 1);
            }
            long length = client.ListLength(key);
            Assert.AreEqual(length, 100);

            string val = client.ListGet(key, 89);
            Assert.AreEqual(val, "10");

            var r = client.ListInsertAfter(key, "10", "y");
            r = client.ListInsertBefore(key, "10", "x");
            length = client.ListLength(key, CommandFlags.DemandMaster);
            Assert.AreEqual(length, 102);
            string[] val_array12 = client.ListRange(key, 85, 102);
            Assert.AreEqual(client.ListGet(key, 89), "x");
            Assert.AreEqual(client.ListGet(key, 90), "10");
            Assert.AreEqual(client.ListGet(key, 91), "y");


            val = client.ListLeftPop(key);
            length = client.ListLength(key, CommandFlags.DemandMaster);
            Assert.AreEqual(length, 101);
            Assert.AreEqual(val, "99");

            val = client.ListRightPop(key);
            length = client.ListLength(key, CommandFlags.DemandMaster);
            Assert.AreEqual(length, 100);
            Assert.AreEqual(val, "0");

            string[] val_array = client.ListRange(key, 95, 98);
            Assert.AreEqual("5", val_array[0]);
            Assert.AreEqual("4", val_array[1]);
            Assert.AreEqual("3", val_array[2]);
            Assert.AreEqual("2", val_array[3]);

            length = client.ListRightPush(key, "n");
            Assert.AreEqual(length, 101);
            Assert.AreEqual("n", client.ListGet(key, 100));

            length = client.ListLeftPush(key, "m");
            Assert.AreEqual(length, 102);
            Assert.AreEqual("m", client.ListGet(key, 0));

            client.ListSetByIndex(key, 0, "test");
            val = client.ListLeftPop(key);
            Assert.AreEqual(val, "test");

            client.ListTrim(key, 0, 49);
            Assert.AreEqual(client.ListLength(key), 50);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestSet()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string set1 = "set1";
            string set2 = "set2";
            string set3 = "set3";
            string set4 = "set4";
            client.KeyRemove(set1);
            client.KeyRemove(set2);
            client.KeyRemove(set3);
            client.KeyRemove(set4);

            client.SetAdd(set1, "1");
            client.SetAdd(set1, "2");
            client.SetAdd(set1, "3");
            client.SetAdd(set2, new string[] { "4", "5", "6" });
            client.SetAdd(set3, new string[] { "7", "8", "9" });

            Assert.AreEqual(client.SetLength(set1), client.SetLength(set2));
            Assert.AreEqual(client.SetLength(set2), 3);

            var result = client.SetCombine(SetOperation.Union, new string[] { set1, set2, set3 });
            Assert.AreEqual(result.Length, 9);
            result = client.SetCombine(SetOperation.Difference, new string[] { set1, set2, set3 });
            Assert.AreEqual(result.Length, 3);
            result = client.SetCombine(SetOperation.Intersect, set1, set2);
            Assert.IsNull(result);

            long length = client.SetCombineAndStore(SetOperation.Intersect, set4, new string[] { set1, set2, set3 });
            Assert.AreEqual(length, 0);
            length = client.SetCombineAndStore(SetOperation.Union, set4, set1, set2);
            Assert.AreEqual(length, 6);

            Assert.IsFalse(client.SetContains(set4, "7"));
            Assert.IsTrue(client.SetContains(set4, "6"));

            result = client.SetMembers(set4);
            Assert.AreEqual(result.Length, 6);

            Assert.IsTrue(client.SetMove(set3, set4, "7"));
            Assert.IsTrue(client.SetContains(set4, "7"));
            Assert.IsFalse(client.SetContains(set3, "7"));

            var s = client.SetPop(client.SetPop(set4));
            Assert.AreEqual(client.SetLength(set4), 6);

            s = client.SetRandomMember(set4);
            Assert.AreEqual(client.SetLength(set4), 6);

            result = client.SetRandomMembers(set4, 4);
            Assert.AreEqual(client.SetLength(set4), 6);
            Assert.AreEqual(result.Length, 4);

            Assert.IsTrue(client.SetRemove(set4, client.SetRandomMember(set4)));
            Assert.AreEqual(client.SetLength(set4), 5);

            length = client.SetRemove(set4, new string[] { "2", "3" });
            Assert.AreEqual(client.SetLength(set4), 5 - length);
        }

        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestSortedSet()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "k_sortedset";

            client.KeyRemove(key);

            for (int i = 1; i < 11; i++)
            {
                client.SortedSetAdd(key, i.ToString(), i);
            }
            Assert.AreEqual(client.SortedSetLength(key), 10);

            var value = client.SortedSetIncrement(key, "10", 1);
            Assert.AreEqual(value, 11);
            value = client.SortedSetDecrement(key, "10", 1);
            Assert.AreEqual(value, 10);

            var result = client.SortedSetRangeByRank(key, 0, 5);
            Assert.AreEqual(result.Length, 6);
            result = client.SortedSetRangeByScore(key, 0, 5);
            Assert.AreEqual(result.Length, 5);

            client.SortedSetRemove(key, "2");
            result = client.SortedSetRangeByRank(key, 0, 5);
            Assert.AreEqual(result.Length, 6);
            result = client.SortedSetRangeByScore(key, 0, 5);
            Assert.AreEqual(result.Length, 4);
        }

        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestServer()
        {
            RedisCacheConfig cacheConfig = RedisConfigManager.GetRedisCacheConfigFromCache();
            IRedisClient client = RedisCacheManager.GetClient<CacheWithBackup>();

            for (int i = 0; i < 100000; i++)
            {
                string key = "s_test_" + i.ToString();
                int slot = GetSlotKey(key);
                RedisClientServer c = client.GetClientServer(key);
                Assert.IsTrue(slot <= c.Node.SlotTo && slot >= c.Node.SlotFrom);
            }

            foreach (var appCache in cacheConfig.ApplicationCaches)
            {
                var pool = RedisConfigManager.GetCachePoolFromCache<CachePoolConfig>(appCache.MainCache);
                foreach (var node in pool.Nodes)
                {
                    var c = client.GetClientServerByNodeName(node.Name);
                    Assert.IsNotNull(c);
                }
            }

            foreach (var appCache in cacheConfig.ApplicationCaches)
            {
                client = RedisCacheManager.GetClient(appCache.Name);
                var pool = RedisConfigManager.GetCachePoolFromCache<CachePoolConfig>(appCache.MainCache);
                var cs = client.GetClientServers();
                Assert.AreEqual(pool.Nodes.Count, cs.Length);
            }

            IRedisClient client1 = RedisCacheManager.GetClient<CacheWithBackup>();
            IRedisClient client2 = RedisCacheManager.GetClient<SingleCache>();
            var info1 = client1.RedisCacheInfo();

            var info2 = client2.RedisCacheInfo();
            Assert.IsNotNull(info1);
            Assert.IsNotNull(info2);

            var t = client1.BackupClient;
            var info3 = client1.RedisCacheInfo();
            Assert.IsNotNull(info3);
            var s = JsonConvert.SerializeObject(info1);

            Assert.IsNotNull(info1.ConnectionCache.BackupConnectionCache);
            Assert.IsNull(info2.ConnectionCache.BackupConnectionCache);

        }

        private int GetSlotKey(string key)
        {
            int h = 0;
            for (int i = 0; i < key.Length; i++)
            {
                h = 33 * h + (int)key[i];
            }
            return Math.Abs(h) % 16384;
        }

        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestHyperLogLog()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key1 = "k_hyperloglog_test_1";
            string key2 = "k_hyperloglog_test_2";
            string key3 = "k_hyperloglog_test_3";
            string key4 = "k_hyperloglog_test_4";
            string key5 = "k_hyperloglog_test_5";
            client.KeyRemove(new string[] { key1, key2, key3, key4, key5 });

            for (int i = 0; i < 50; i++)
            {
                client.HyperLogLogAdd(key1, i.ToString());
            }
            List<string> list = new List<string>();
            for (int i = 50; i < 100; i++)
            {
                list.Add(i.ToString());
            }
            client.HyperLogLogAdd(key1, list.ToArray());

            var length = client.HyperLogLogLength(key1);
            Assert.AreEqual(length, 100);

            for (int i = 0; i < 100; i++)
            {
                client.HyperLogLogAdd(key2, i.ToString());
            }

            client.HyperLogLogMerge(key3, key1, key2);
            Assert.AreEqual(client.HyperLogLogLength(key2), 100);
            Assert.AreEqual(client.HyperLogLogLength(key3), 100);

            for (int i = 100; i < 200; i++)
            {
                client.HyperLogLogAdd(key4, i.ToString());
            }

            client.HyperLogLogMerge(key5, new string[] { key1, key4 });
            Assert.AreEqual(client.HyperLogLogLength(key4), 100);
            //Assert.AreEqual(client.HyperLogLogLength(key5), 200);
        }

        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestBitmaps()
        {
            IRedisClient client = RedisCacheManager.GetClient<SingleCache>();
            string key = "k_bitmaps_test";
            string key1 = "k_bitmaps_test_1";
            string key2 = "k_bitmaps_test_2";
            string key3 = "k_bitmaps_test_3";
            string key4 = "k_bitmaps_test_4";
            client.KeyRemove(new string[] { key, key1, key2, key3, key4 });

            //10101010
            for (int i = 0; i < 16; i++)
            {
                var bit = i % 2 == 0;
                client.StringSetBit(key, i, bit);
            }

            Assert.IsTrue(client.StringGetBit(key, 0));
            Assert.IsFalse(client.StringGetBit(key, 1));

            Assert.AreEqual(client.StringBitPosition(key, true), 0);
            Assert.AreEqual(client.StringBitPosition(key, false), 1);

            Assert.AreEqual(client.StringBitCount(key), 8);

            //01010101
            client.StringBitOperation(Bitwise.Not, key1, new string[] { key });
            Assert.AreEqual(client.StringBitCount(key1), 8);
            //00000000
            client.StringBitOperation(Bitwise.And, key2, new string[] { key, key1 });
            Assert.AreEqual(client.StringBitCount(key2), 0);
            //11111111
            client.StringBitOperation(Bitwise.Or, key3, new string[] { key, key1 });
            Assert.AreEqual(client.StringBitCount(key3), 16);
            //11111111
            client.StringBitOperation(Bitwise.Xor, key4, new string[] { key, key1 });
            Assert.AreEqual(client.StringBitCount(key4), 16);
        }

        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_1.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Redis_CacheManager.config", @"Configs")]
        [DeploymentItem(@"Configs\Cache\multiple_cache_idc_2.xml", @"Configs\Cache")]
        [DeploymentItem(@"Configs\Cache\single_cache.xml", @"Configs\Cache")]
        [TestMethod]
        public void TestDataSync()
        {
            IRedisClient client = RedisCacheManager.GetClient("cacheWithBackup");
            IRedisClient backupClient = client.BackupClient;

            if (backupClient == null) Assert.Fail("need backupClient");

            string key = "sync_test_";
            client.KeyRemove(key);
            backupClient.KeyRemove(key);

            for (int i = 0; i < 100; i++)
            {
                client.StringSet(key + i.ToString(), i.ToString());
            }

            for (int i = 0; i < 100; i++)
            {
                string val = backupClient.StringGet(key + i.ToString());
                Assert.AreEqual(val, i.ToString());
            }
        }
    }

    #region extension

    /// <summary>
    /// 用户缓存约定搜索key=userCache的缓存
    /// </summary>
    public class CacheWithBackup : ApplicationCacheConfig
    {
        public override string GetAppCacheName()
        {
            return "cacheWithBackup";
        }
    }
    /// <summary>
    /// 用户缓存约定搜索key=intercache的缓存
    /// </summary>
    public class SingleCache : ApplicationCacheConfig
    {
        public override string GetAppCacheName()
        {
            return "singleCache";
        }
    }

    #endregion
}
