﻿using gt.rediscache;
using gt.rediscache.core;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace gt.rediscachemanager.swithtest
{
    class Program
    {
        /// <summary>
        /// 检查点：
        /// 1.单节点 slave down--->restored
        /// 2.单节点 master down---->restored
        /// 3.单节点 master slave all down
        /// 4.多节点 sync
        /// 5.多节点 主pool slave down
        /// 6.多节点 主pool master down,slave turn to master
        /// 7.多节点 主pool master down,slave not change
        /// 8.多节点 主pool master and slave down
        /// 9.多节点 初始化时 主pool down
        /// 10.多节点 备pool slave down
        /// 11.多节点 备pool master down
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            log4net.Config.XmlConfigurator.ConfigureAndWatch(new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "log4net.config"));
            RedisCacheManager.InitCacheFromConfig();

            SwitchTest();

            Console.ReadLine();
        }

        static void SwitchTest()
        {
            IRedisClient client = RedisCacheManager.GetClient("cacheWithBackup");
            //IRedisClient client = RedisCacheManager.GetClient("singleCache");
            string key = "switch_hash_test";
            for (int i = 0; i < 10000; i++)
            {
                try
                {
                    if (i == 30)
                    {
                        //close maincache
                    }
                    client.HashSet(key, i.ToString(), i.ToString());
                    Thread.Sleep(1);
                }
                catch
                {
                    Console.WriteLine("hashset error.");
                }
            }

            client.KeyRemove(key);

            Console.WriteLine("done.");
        }
    }
}
