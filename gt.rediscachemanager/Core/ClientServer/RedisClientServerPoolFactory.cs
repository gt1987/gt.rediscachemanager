﻿using gt.rediscachemanager.Entry;
using System.Collections.Concurrent;

namespace gt.rediscachemanager.Core.ClientServer
{
    internal class RedisClientServerPoolFactory
    {
        private static ConcurrentDictionary<string, RedisClientServerPool> m_container = new ConcurrentDictionary<string, RedisClientServerPool>();

        public static RedisClientServerPool GetOrCreate(CachePoolConfiguration configuration)
        {
            return m_container.GetOrAdd(configuration.Name, (x) => { return new RedisClientServerPool(configuration); });
        }
    }
}
