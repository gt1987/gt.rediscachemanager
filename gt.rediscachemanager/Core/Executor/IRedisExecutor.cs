﻿using gt.rediscachemanager.Entry;
using StackExchange.Redis;
using System.Threading.Tasks;

namespace gt.rediscachemanager.Core.Executor
{
    public interface IRedisExecutor
    {
        EMRedisResult ExecuteCommand(RedisCommand command, EMRedisMessage message, CommandFlags commandFlags);
        Task<EMRedisResult> ExecuteCommandAsync(RedisCommand command, EMRedisMessage message, CommandFlags commandFlags);
    }
}
