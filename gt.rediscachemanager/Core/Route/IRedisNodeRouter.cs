﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gt.rediscachemanager.Core.Route
{
    internal interface IRedisNodeRouter
    {
        /// <summary>
        /// 获取key指向的nodename
        /// </summary>
        string GetNodeName(string key);
    }
}
