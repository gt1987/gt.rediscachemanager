﻿using System;

namespace gt.rediscachemanager.Core.Route
{
    public class SentinelNodeRouter : IRedisNodeRouter
    {
        public string GetNodeName(string key)
        {
            throw new Exception("sentinel mode not support get node name by key.");
        }
    }
}
