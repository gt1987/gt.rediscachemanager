﻿using gt.rediscachemanager.Entry;
using System;

namespace gt.rediscachemanager.Core.Route
{
    internal class SingleNodeRouter : IRedisNodeRouter
    {
        private RouteData m_route = null;
        public SingleNodeRouter(RouteData route)
        {
            if (route == null || string.IsNullOrEmpty(route.NodeName)) throw new ArgumentNullException("route or route.NodeName");
            this.m_route = route;
        }
        public string GetNodeName(string key)
        {
            return this.m_route.NodeName;
        }
    }
}
