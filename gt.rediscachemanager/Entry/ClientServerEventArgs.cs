﻿using System;
using System.Net;

namespace gt.rediscachemanager.Entry
{
    public class ClientServerConnectionFailedEventArgs : EventArgs
    {
        public EndPoint ServerEndPoint { get; set; }
        public bool IsMasterServer { get; set; }
    }

    public class ClientServerConnectionRestoredEventArgs : EventArgs
    {
        public EndPoint ServerEndPoint { get; set; }
        public bool IsMasterServer { get; set; }
    }

    public class MonitorConnectionFailedEventArgs : EventArgs
    {
        public string ClientName { get; set; }
        public string CachePoolName { get; set; }
        public RedisNode Node { get; set; }
    }

    public class MonitorConnectionRestoredEventArgs : EventArgs
    {
        public string ClientName { get; set; }
        public string CachePoolName { get; set; }
        public RedisNode Node { get; set; }
    }
}
