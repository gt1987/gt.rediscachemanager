﻿using gt.rediscachemanager.Utility;
using StackExchange.Redis;
using System.Collections.Generic;
using System.Text;

namespace gt.rediscachemanager.Entry
{
    public class RedisClientConfiguration
    {
        public string ApplicationName { get; set; }
        public string IDC { get; set; }
        public string ClientName { get; set; }
        public CachePoolConfiguration Pool { get; set; }
    }

    /// <summary>
    /// 缓存池信息
    /// </summary>
    public class CachePoolConfiguration
    {
        public string Name { get; set; }
        public PoolMode Mode { get; set; }
        public List<RedisNode> Nodes { get; set; }
    }

    public class RedisNode
    {
        public string Name { get; set; }
        public ConfigurationOptions RedisConnectOptions { get; set; }
        public string Master { get; set; }
        public List<string> Slaves { get; set; }
        /// <summary>
        /// slot From
        /// useful in Multipler
        /// </summary>
        public int SlotFrom { get; set; }
        /// <summary>
        /// slot End
        /// useful in Multipler
        /// </summary>
        public int SlotTo { get; set; }
        public int DB { get; set; }
        public override string ToString()
        {
            if (RedisConnectOptions == null || RedisConnectOptions.EndPoints == null) return string.Empty;
            StringBuilder s = new StringBuilder(Name);
            s.Append("(");
            s.Append(RedisClientUtility.GetHostWithPort(RedisConnectOptions.EndPoints));
            s.Append(")");
            return s.ToString();
        }
    }

    public class SmartRedisClientConfiguration : RedisClientConfiguration
    {
        public bool AllowSwitch { get; set; }
        public bool AllowSync { get; set; }
        public int? DelayedRecoverySeconds { get; set; }
        public int? RedisFailoverWaitSeconds { get; set; }
        public SmartRedisClientConfiguration BackupConfiguration { get; set; }
    }

    public enum PoolMode
    {
        /// <summary>
        /// 默认单点
        /// </summary>
        Single,
        /// <summary>
        /// 多点主从
        /// </summary>
        Multiple,
        /// <summary>
        /// Sentinel模式
        /// </summary>
        Sentinel
    }

    public enum BackupModeEnum
    {
        /// <summary>
        /// 默认 不切换也不同步
        /// </summary>
        None = 0,
        /// <summary>
        /// 主备切换
        /// </summary>
        Switch = 1,
        /// <summary>
        /// 同步数据
        /// </summary>
        Sync = 2,
        /// <summary>
        /// 主备切换且同步数据
        /// </summary>
        SwitchAndSync = 3
    }
}
