﻿namespace gt.rediscachemanager.Entry
{
    internal class RouteData
    {
        public string NodeName { get; set; }
        public int SlotFrom { get; set; }
        public int SlotTo { get; set; }
    }
}
