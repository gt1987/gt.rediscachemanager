﻿using gt.rediscachemanager.Entry;
using gt.rediscachemanager.Impl.RedisClient;
using StackExchange.Redis;

namespace gt.rediscachemanager.Handlers
{
    public class SyncHandler
    {
        public SyncHandler()
        { }

        public void SyncMessage(SmartRedisClient toRedisClient, RedisCommand command, EMRedisMessage message)
        {
            try
            {
                if (toRedisClient != null && message.GetCommandFlags(command) == CommandFlags.DemandMaster)
                {
                    var clientServer = toRedisClient.GetSmartClientServer(message.Key).Item1;
                    clientServer.ExecuteCommand(command, message, CommandFlags.FireAndForget);
                }
            }
            catch { }
        }
    }
}
