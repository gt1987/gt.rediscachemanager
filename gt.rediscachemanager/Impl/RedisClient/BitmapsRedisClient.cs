﻿using gt.rediscachemanager.Entry;
using gt.rediscachemanager.Utility;
using StackExchange.Redis;
using System;
using System.Threading.Tasks;

namespace gt.rediscachemanager.Impl.RedisClient
{
    public partial class RedisClient
    {
        public long StringBitCount(string key, long start = 0, long end = -1, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringBitCountMessage(key, start, end);
            return Execute(RedisCommand.StringBitCount, message, flags).LongResult;
        }

        public long StringBitOperation(StackExchange.Redis.Bitwise operation, string destination, string[] keys, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            if (this.Pool.Mode != PoolMode.Single)
                throw new InvalidOperationException("this operation do not support by cluster mode!");
            var redisKeys = RedisValueUtility.ConvertToRedisKeyFromString(keys);
            var message = new StringBitOperationArrayMessage(destination, operation, redisKeys);
            return Execute(RedisCommand.StringBitOperationArray, message, flags).LongResult;
        }

        public long StringBitOperation(StackExchange.Redis.Bitwise operation, string destination, string first, string second = null, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            if (this.Pool.Mode != PoolMode.Single)
                throw new InvalidOperationException("this operation do not support by cluster mode!");
            var message = new StringBitOperationMessage(destination, operation, first, second);
            return Execute(RedisCommand.StringBitOperation, message, flags).LongResult;
        }

        public long StringBitPosition(string key, bool bit, long start = 0, long end = -1, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringBitPositionMessage(key, bit, start, end);
            return Execute(RedisCommand.StringBitPosition, message, flags).LongResult;
        }

        public bool StringGetBit(string key, long offset, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringGetBitMessage(key, offset);
            return Execute(RedisCommand.StringGetBit, message, flags).BoolResult;
        }

        public bool StringSetBit(string key, long offset, bool bit, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringSetBitMessage(key, bit, offset);
            return Execute(RedisCommand.StringSetBit, message, flags).BoolResult;
        }

        #region Async

        public async Task<long> StringBitCountAsync(string key, long start = 0, long end = -1, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringBitCountMessage(key, start, end);
            return await ExecuteAsync(RedisCommand.StringBitCount, message, flags).ContinueWith(t =>
            {
                return t.Result.LongResult;
            }).ConfigureAwait(false);
        }

        public async Task<long> StringBitOperationAsync(StackExchange.Redis.Bitwise operation, string destination, string[] keys, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            if (this.Pool.Mode != PoolMode.Single)
                throw new InvalidOperationException("this operation do not support by cluster mode!");
            var redisKeys = RedisValueUtility.ConvertToRedisKeyFromString(keys);
            var message = new StringBitOperationArrayMessage(destination, operation, redisKeys);
            return await ExecuteAsync(RedisCommand.StringBitOperationArray, message, flags).ContinueWith(t =>
            {
                return t.Result.LongResult;
            }).ConfigureAwait(false);
        }

        public async Task<long> StringBitOperationAsync(StackExchange.Redis.Bitwise operation, string destination, string first, string second = null, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            if (this.Pool.Mode != PoolMode.Single)
                throw new InvalidOperationException("this operation do not support by cluster mode!");
            var message = new StringBitOperationMessage(destination, operation, first, second);
            return await ExecuteAsync(RedisCommand.StringBitOperation, message, flags).ContinueWith(t =>
            {
                return t.Result.LongResult;
            }).ConfigureAwait(false);
        }

        public async Task<long> StringBitPositionAsync(string key, bool bit, long start = 0, long end = -1, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringBitPositionMessage(key, bit, start, end);
            return await ExecuteAsync(RedisCommand.StringBitPosition, message, flags).ContinueWith(t =>
            {
                return t.Result.LongResult;
            }).ConfigureAwait(false);
        }

        public async Task<bool> StringGetBitAsync(string key, long offset, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringGetBitMessage(key, offset);
            return await ExecuteAsync(RedisCommand.StringGetBit, message, flags).ContinueWith(t =>
            {
                return t.Result.BoolResult;
            }).ConfigureAwait(false);
        }

        public async Task<bool> StringSetBitAsync(string key, long offset, bool bit, StackExchange.Redis.CommandFlags flags = CommandFlags.None)
        {
            var message = new StringSetBitMessage(key, bit, offset);
            return await ExecuteAsync(RedisCommand.StringSetBit, message, flags).ContinueWith(t =>
            {
                return t.Result.BoolResult;
            }).ConfigureAwait(false);
        }

        #endregion
    }
}
