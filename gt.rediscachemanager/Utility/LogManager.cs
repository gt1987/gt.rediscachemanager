﻿using System;

namespace gt.rediscachemanager.Utility
{
    internal sealed class LogUtility
    {
        private static log4net.ILog _defaultLogger = log4net.LogManager.GetLogger("RedisLogger");

        public static void SetLogger(log4net.ILog logger)
        {
            if (logger != null)
                _defaultLogger = logger;
        }

        /// <summary>
        /// 记录调试日志信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Debug(string msg)
        {
            if (_defaultLogger.IsDebugEnabled)
                _defaultLogger.Debug(msg);
        }

        public static void Warn(string msg)
        {
            if (_defaultLogger.IsWarnEnabled)
                _defaultLogger.Warn(msg);
        }

        /// <summary>
        /// 记录信息日志信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Info(string msg)
        {
            if (_defaultLogger.IsInfoEnabled)
                _defaultLogger.Info(msg);
        }

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg"></param>
        public static void Error(string msg)
        {
            Error(msg, null);
        }

        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="e"></param>
        public static void Error(Exception e)
        {
            Error(null, e);
        }
        /// <summary>
        /// 记录错误日志信息
        /// </summary>
        /// <param name="msg">错误信息</param>
        /// <param name="e"></param>
        public static void Error(string msg, Exception e)
        {
            if (_defaultLogger.IsErrorEnabled)
                _defaultLogger.Error(msg, e);
        }
    }
}
